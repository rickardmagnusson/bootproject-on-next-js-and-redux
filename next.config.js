const routes = require("./src/lib/RouteBuilder.js");

/*
 * Configuration for Next to build all 
 * routes to create static routes from the api. 
 * See routeBuilder module. 
 */
module.exports = {

    poweredByHeader: false,

    /*Create index folders*/
    exportTrailingSlash: true,

    useFileSystemPublicRoutes: false,

    exportPathMap: async function () {
        /*Create paths from RouteBuilder*/
        return await routes.RouteBuilder();
    },
    generateBuildId: async () => {
        return process.env.NODE_ENV === 'production' ? 'production' : 'dev';
    }
    //This configuration try to create script links to pages, but fails??
    //,
    //serverRuntimeConfig: { // Will only be available on the server side
    //    staticFolder: 'static' //This can't be changed?
    //}
    ,
    publicRuntimeConfig: {
        DOMAIN: process.env.NODE_ENV === 'production' ? 'https://www.bootproject.no' : 'http://localhost:3000',
        staticFolder: 'static',
        assetPrefix: process.env.NODE_ENV === 'production' ? "/projects" : ""
    }
};
﻿# React JS, Redux Next JS


![bootproject.png](SolutionItems/bootproject.png)

## Description of project

    This project is the bootproject website made with 
    React, Next js and Redux.
    Generate dynamic components, generated from the web api. 
   
 A working demo can be seen here: [Bootproject website](https://www.bootproject.no)
    
## Zones
    Put content anywhere you want. Eg.
    <Zone name="Content" />
    <Zone name="AfterContent" />

## Page Zones and modules

    To create a zone for a page, you add as shows below.

```
#!c# 
pages: [
        {
            id: 1,
            title: "Home",
            meta_title: "Page title",
            path: "/",
            meta_description: "Meta description",
            meta_keywords: "Meta keywords separated by comma",
            layout: "/index",
            template: "template.home",
            zones: [
                {
                    name: "BeforeContent", // The zone name
                    modules: ["Content"] // Modules that loads into the zone
                },
                {
                    name: "Content",
                    modules: ["Content", "Timeline"]
                },
                {
                    name: "AfterContent",
                    modules: ["Content"]
                }]
        }
    ]
```
     
## Define a module
    
```
#!c#
content: [
                {
                id: "1",
                path: "/", //Important for the page to load this module
                zone: "Content", //Important for the page to load this module
                style: "",
                title: "Content title",
                content: "Some content",
                icon: "Path to icon",
                list: [],
                //Below is an optional list

                list: [
                    {
                        icon: "path to icon",
                        title: "The title",
                        content: "Description"
                    },
                    {
                        icon: "Path to icon",
                        title: "The title",
                        content: "Description"
                    },
                    {
                        icon: "Path to icon",
                        title: "The title",
                        content: "Description"
                    }
                ]
            }
```

## Goal
    A Next JS static site that loads / reloads data from a web api.
    Uses Redux as datastore.
  
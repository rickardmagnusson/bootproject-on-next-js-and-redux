﻿const sanityClient = require('@sanity/client');

const client = sanityClient({
    projectId: '68ix3g15',
    dataset: 'production',
    token: '', 
    useCdn: false
});

module.exports = client;

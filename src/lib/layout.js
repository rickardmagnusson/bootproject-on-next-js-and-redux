import React from 'react';
import Meta from "../lib/meta";

/* 
 * Handles the meta tags on each page post etc.
 */
const Layout = (props) => {

    return (
        <React.Fragment>
            <Meta current={props.current} meta={{...props.meta}} />
            {props.children}
        </React.Fragment>
    );
};

export default Layout;
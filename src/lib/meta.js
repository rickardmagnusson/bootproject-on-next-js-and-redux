/* eslint-disable */
import React from 'react';
import Head from 'next/head';

/*
 * Generate meta tags
 */
const Meta = (props) => {
    if (!props.meta) {
        console.error('no meta');
    }

    /**
     * Construct keywords if none
     * This will do for now since Im to lazy to write each keyword tag.
     */
    var keywords = () => {
       
        if (props.meta)
            if (props.meta.keywords) {
                return this.props.meta.keywords;
            } else {
                if(props.meta.title)
                return props.meta.title.replace(new RegExp(" ", 'g'), ",");
            }
    }

    return (
            <Head>
                <meta charSet="utf-8" />
                {props.meta.title && <title>{props.meta.title}</title>}
                {props.meta.description && <meta name="description" content={props.meta.description} />}
                <meta name="keywords" content={keywords()} />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no" />
                <meta name="apple-mobile-web-app-title" content="Bootproject" />
                <meta name="apple-mobile-web-app-capable" content="yes" />
                <meta name="mobile-web-app-capable" content="yes" />
                <meta property="og:url" content={"https://www.bootproject.no" + props.current} />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Bootproject" />
                {props.meta.description && <meta property="og:description" content={props.meta.description} />}
                <meta property="og:image" content="https://www.bootproject.no/static/favicon.png" />
                <link rel="manifest" href="/static/manifest.json" />
                <link rel="apple-touch-icon" sizes="57x57" href="/static/img/apple-icon-57x57.png" />
                <link rel="apple-touch-icon" sizes="72x72" href="/static/img/apple-icon-72x72.png" />
                <link rel="apple-touch-icon" sizes="114x114" href="/static/img/apple-icon-114x114.png" />
                <link rel="apple-touch-icon" sizes="144x144" href="/static/img/apple-icon-144x144.png" />
                <link rel="apple-touch-startup-image" href="/static/img/apple-touch-startup-image-320x460.png" media="(device-width: 320px)" />
                <link rel="apple-touch-startup-image" href="/static/img/apple-touch-startup-image-640x920.png" media="(device-width: 320px) and (device-height: 460px) and (-webkit-device-pixel-ratio: 2)" />
                <link rel="apple-touch-startup-image" href="/static/img/apple-touch-startup-image-640x1096.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" />
                <link rel="apple-touch-startup-image" href="/static/img/apple-touch-startup-image-828x1792.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" />
                <link rel="apple-touch-startup-image" href="/static/img/apple-touch-startup-image-828x1792.png" />
                <link rel="stylesheet" href="/static/css/all.css" />
                <link rel="stylesheet" href="/static/fonts/calibre/calibre.css" />
                <link rel="stylesheet" href="/static/css/bootstrap.min.css" />
                <link rel="stylesheet" href="/static/css/bootproject.css" />
                <link rel="stylesheet" href="/static/css/font-awesome.min.css" />
                <link rel="stylesheet" href="/static/css/mobilemenu.css" />
                <link rel="stylesheet" href="/static/css/subnav.css" />
                <link rel="icon" href="/static/favicon.png" type="image/x-icon" />
                <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145353906-2" />
                <script dangerouslySetInnerHTML={{ __html: ` window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments)} gtag('js', new Date());gtag('config', 'UA-145353906-2'); `}} />
            </Head>
    );
};

export default Meta;
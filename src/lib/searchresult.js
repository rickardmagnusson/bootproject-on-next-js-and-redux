﻿
/*
 * Searchresult helper 
 */
const SearchResult = (q, list, excerptCount) => {

    var results = [];
    var result = filter(q, list);

    result.map(c =>
        results.push({ path: c.path, title: c.title, content: c.content.substr(0, excerptCount)+'...' , domain:  'https://www.bootproject.no', type: c.type}));

    return results;

    function escapeRegExp(s) {
        return s.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\$&");
    }

    function filter(q, list) {
        const words = q
            .split(/\s+/g)
            .map(s => s.trim())
            .filter(s => !!s);

        const hasTrailingSpace = q.endsWith(" ");
        const searchRegex = new RegExp(
            words
                .map((word, i) => {
                    if (i + 1 === words.length && !hasTrailingSpace) {
                        // The last word - ok with the word being "startswith"-like
                        return `(?=.*\\b${escapeRegExp(word)})`;
                    } else {
                        // Not the last word - expect the whole word exactly
                        return `(?=.*\\b${escapeRegExp(word)}\\b)`;
                    }
                })
                .join("") + ".+",
            "gi"
        );
        return list.filter(item => {
            return searchRegex.test(item.content);
        });
    }
};

export default SearchResult;
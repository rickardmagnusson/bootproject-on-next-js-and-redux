﻿import { createStore } from "redux";
import initialApi from "../api";

export const actionTypes = {};

// REDUCERS
const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export function initializeStore(initialState = initialApi) {
    return createStore(
        rootReducer,
        initialState);
}
const Api = require("../api");

/*
 * Route object
 */
var Route = (function () {
    function Route() {
    }
    return Route;
}());

/*
 * Constructs routes from pages and project
 */
module.exports = {

    RouteBuilder: async function () {

        var Routes = new Array();

        Api.pages.map(function (p) {
            var r = new Route();
            r.id = p.id;
            r.path = p.path;
            r.pathId = "";
            r.layout = p.layout;
            r.meta_title = p.meta_title;
            r.meta_description = p.meta_description;
            Routes.push(r);
        });

         Api.projects.posts.map(function (p) {
            var r = new Route();
            r.id = p.id;
            r.path = "/projects/" + p.slug;
            r.pathId = p.slug;
            r.layout = p.layout;
            r.meta_title = p.meta_title;
            r.meta_description = p.meta_description;
            Routes.push(r);
        });

        const pages = Routes.reduce(
            (pages, Route) =>
                Object.assign({}, pages, {
                    [`${Route.path}`]: {
                        page: Route.layout,
                        query: {
                            pathName: Route.path,
                            path: Route.path,
                            pathId: Route.pathId,
                            meta_title: Route.meta_title,
                            meta_description: Route.meta_description
                        }
                    }
                }),
            {}
        );
        return Object.assign({}, pages, {});
    }
};
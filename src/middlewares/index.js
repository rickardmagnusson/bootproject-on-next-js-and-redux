﻿const Routes = require("../middlewares/routes");
const Contents = require("../middlewares/content");
const Projects = require("../middlewares/projects");
const Search = require("../middlewares/search");

module.exports = async () => {
    return {
        service: {
            log: await Routes().then(c => { console.log(c); }),
            pages: await Routes().then(c => { return c;}),
            content: await Contents().then(c => { return c; }),
            projects: await Projects().then(c => { return c; }),
            search: await Search().then(c => { return c; })
        }
    };
};
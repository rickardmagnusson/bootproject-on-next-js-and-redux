﻿const client = require("../lib/sanity");

module.exports = async () => {

    var data = {
        content: []
    };

    const query =
        `*[_type == "content"]{
          content,
           title,
            style,
            zones,
            icon,
            list,
          "path": path[]->slug.current
        }`;

    await client.fetch(query)
        .then(blocks => {
            blocks.map(p => {
                data.content.push({
                    title : p.title,
                    style : p.style,
                    content : "test",
                    path : p.path[0],
                    zone : p.zones[0].name[0]
                });
            });
        });

    console.log(data);

    return data;
}
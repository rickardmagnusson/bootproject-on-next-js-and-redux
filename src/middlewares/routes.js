﻿const client = require("../lib/sanity");


module.exports = async () => {

    var data = {
        pages: []
    };

    const query = `*[_type == "page" && published == true] | order(sortorder){
              _id,title, meta_title, meta_description, meta_keywords, slug, 
              layout, zones
    }`;

    await client.fetch(query)
        .then(pages => {
            pages.map(p => {
                var r = {
                    id: p._id,
                    title: p.title,
                    path: p.slug.current,
                    pathId: "",
                    layout: p.layout[0],
                    meta_title: p.meta_title,
                    meta_description: p.meta_description,
                    keywords: p.meta_keywords,
                    hidden: p.hidden === undefined ? false : true,
                    zones: p.zones.map(n => {
                        return { name: n.name[0], modules: n.modules.map(m => { return m; }) };
                    })
                };
                data.pages.push(r);
            });
        });

    console.log(data);

    return data;
};
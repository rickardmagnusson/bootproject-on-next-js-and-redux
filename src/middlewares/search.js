﻿const client = require("../lib/sanity");

module.exports = async () => {

    const query = `*[ _type == "search" ]{ ..., "path": path->slug.current }`; 

    await client.fetch(query)
        .then(blocks => {
            blocks.map(p => {
                var s = {
                    search: {
                        zone: p.zone[0],
                        path: p.path,
                        includeProps: p.includeProps
                    }
                };
                return s;
            });
        });
}
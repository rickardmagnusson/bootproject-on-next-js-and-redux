﻿const client = require("../lib/sanity");

module.exports = async () => {

    var data = {
        projects: {
            title: "",
            posts: []
        }
    };

    const query =
        `*[ _type == "timeline" ]{ ..., posts-> }`;

    await client.fetch(query)
        .then(blocks => {
            blocks.map(p => {

                data.projects.title = p.title;

                if (p.posts)
                {
                    p.posts.map(post => {
                        var r = {
                            slug: post.slug,
                            sortorder: post.sortorder,
                            created: post.publishedAt,
                            content: "projecttext",
                            summary: post.summary,
                            code: post.code
                        };
                        data.projects.posts.push(r);
                    });
                }
            });
        });

    console.log(data);

    return data;
}
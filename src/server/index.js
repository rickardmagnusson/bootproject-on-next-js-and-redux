﻿const next = require('next');
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {

    // ℹ️ Below is the magic which forces a page reload on every change
    // ℹ️ From https://github.com/zeit/next.js/issues/1109#issuecomment-446841487

    // The publish function tells the client when there's a change that should be hot reloaded
    const publish = app.hotReloader.webpackHotMiddleware.publish.bind(
        app.hotReloader.webpackHotMiddleware,
    );

    // Intercept publish events so we can send something custom
    app.hotReloader.webpackHotMiddleware.publish = (event, ...rest) => {
        let forwardedEvent = event;

        // upgrade from a "change" to a "reload" event to trick the browser into reloading
        if (event.action === 'change') {
            forwardedEvent = {
                action: 'reload',
                // Only `/_document` pages will trigger a full browser refresh, so we force it here.
                data: ['/_document']
            };
        }

        // Forward the (original or upgraded) event on to the browser
        publish(forwardedEvent, ...rest);
    };
    // ℹ️ End force page reload on change

    // ... Whatever custom server setup you have. See: https://github.com/zeit/next.js/#custom-server-and-routing
    app.listen(3000, () => console.log('Example app listening on port 3000!'));

});


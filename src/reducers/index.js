﻿import { SET_FETCH_DATA } from "../types";

export default function (state = {}, action) {
    switch (action.type) {
        case SET_FETCH_DATA:
            return { data: action.payload };
        default:
            return state;
    }
}
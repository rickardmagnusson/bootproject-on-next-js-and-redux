﻿/* eslint-disable */
import React from "react";
import SearchResult from "../lib/searchresult";

/*
 * Search module.
 */
export default class Search extends React.Component{

    constructor(props) {
        super();

        this.state = {
            data: props.props,
            results: [{ title: "", content: "", path: "", domain: '', type: ''}]
        };

        this.getResults = this.getResults.bind(this);
    }

    getResults(e) {
        var elements = [];

        /*Push element to a common array*/
        this.state.data.content.map(c =>
            elements.push({ path: c.path, title: c.title, content: c.content.replace(/<[^>]*>?/gm, ''), domain: 'https://www.bootproject.no', type: 'In Content:'}));

        this.state.data.projects.posts.filter(c =>
            elements.push({ path: "/projects/" + c.slug, title: c.title, content: c.content.replace(/<[^>]*>?/gm, ''), domain: 'https://www.bootproject.no', type: 'In Projects:'}));

        /*Create results*/
        var searchResults = SearchResult(e, elements, 150);

        this.setState({ results: searchResults });
    }

    render() {
        const { results } = this.state;

        return(
        <div className="sectionroot">
            <section>
                <div className="sectioncontent">
                    <div className="sharedSection">
                        <h1>Search</h1>
                        <p>Enter any keyword to search on bootproject.</p>
                        <div className="input-group mb-3">
                                <input id="search" type="text" onChange={result => this.getResults(result.target.value)} className="form-control" placeholder="Search..." />
                            </div>
                            <ul className="searchList">
                                {results.map((c, i) => <li key={"searchresult-" + i}><a href={c.path}>{c.title}</a><br /><em>{c.content}</em><br />{c.type} {c.domain}{c.path}<br/><br/></li>)}
                            </ul>
                    </div>
                </div>
             </section>
        </div>
        );
    }
}
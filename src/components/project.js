/* eslint-disable */
import React from "react";
import SyntaxHighlighter from 'react-syntax-highlighter';
import Link from "next/link";
import SubNav from "./subnav";

/*
 * Constructs project content with 
 * SyntaxHighlighter or pure content.
 */
export default ({ props, route, posts, title }) => (
    <div className="sectionroot">
        <section>
            <div className="sharedSection">
                <div className="sectioncontent">
                    <SubNav posts={posts} slug={props.slug} title="Projects &raquo;" />
                    <div>
                        <div className="row">
                            <div className="col-lg-9 content">
                                <h1>{props.title}</h1>
                                <span dangerouslySetInnerHTML={{ __html: props.content }} />
                                <br /><br />
                                {props.code && <SyntaxHighlighter language="javascript">{props.code}</SyntaxHighlighter>}
                                <br />

                                {props.summary && <span dangerouslySetInnerHTML={{ __html: props.summary }} />}

                                <hr className="dotted" />
                                <em>Created by Bootproject editor on {props.created}</em> <br /><br /><br />
                               
                                <div id="disqus_thread"></div>

                                <script dangerouslySetInnerHTML={{
                                    __html: `
                                    var disqus_config = function () {
                                        this.page.url =  "https://www.bootproject.no/projects/` + props.slug + `;"
                                        this.page.identifier = "`+ props.slug +`"; 
                                    }`}} />
                               
                            </div>
                            <div className="col-lg-3 sidebar timeliner"><h2>{title}</h2>
                                {posts.map((t, i) => <div key={"pointer"+ i} className='timeline-point'><div className='timeline-icon'></div>
                                    <div key={"blocked-" + i} className='timeline-block'>
                                        <Link key={t.id} href={"/projects/" + t.slug}><a className={t.slug === props.slug ? "active" : "none"}>{t.title}<br /><em>Created on {t.created}</em></a></Link>
                                     </div>
                                 </div>)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
);
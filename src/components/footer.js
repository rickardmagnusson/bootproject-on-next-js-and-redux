/* eslint-disable */
import React from "react";

/*
 * Footer, static content
 */
export default (props) => (
    <footer>
        <section className="sharedSection">
            <div className="sectioncontent">
                <ul className="shared_grid">
                    <li>
                        <img src="/static/img/user.white.png" alt="multi" className="mini" /> <h2>About</h2>
                        <p>The Swede(me), Rickard Magnusson a Senior Software Engineer, currently employeed as a consultant in Norway.</p>
                    </li>
                    <li>
                        <h2>Legal</h2>
                        <p>Bootproject.se and Footer.se is property of Bootproject and may be used under (MIT)licence . See project on Bitbucket</p>
                    </li>
                    <li>
                        <h2><img src="/static/img/logo.svg" alt="Logo" className="small" /> Contact</h2>
                        <p>Email: bootproject@icloud.com</p>
                    </li>
                </ul>
                <div>(&copy;) Copyright www.bootproject.no 2019</div>
            </div>
        </section>
    </footer>
);

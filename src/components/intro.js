/* eslint-disable */
import React from "react";

/*
 * The default content on startpage, static.
 */
export default (props) => (
    <div className="sectionroot">
        <section>
            <div className="sectioncontent">
                <div className="sharedSection">
                    <h1><span>Boot</span><span>project </span><span>-developer</span><span>connection</span></h1>
                    <div className="imagecontainer">
                        <img alt="Logo back" src="/static/img/communication.svg" />
                    </div>
                    <div>
                        <p className="intro">Bootproject is development base for all projects and also where to create and test out new features from both .NET, javascript frameworks and more.<br /></p>
                        <a className="biglink" href="https://bitbucket.org/rickardmagnusson/bootproject-on-next-js-and-redux/src/master/" rel="noopener noreferrer" target="_blank">Grab your copy V2 on bitbucket</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
)

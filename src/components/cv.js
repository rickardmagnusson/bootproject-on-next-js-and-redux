﻿/* eslint-disable */
import React from "react";
import SyntaxHighlighter from 'react-syntax-highlighter';
import Link from "next/link";
import SubNav from "./subnav";

/*
 * Constructs cv content
 */
export default () => (
    <div className="sectionroot">
        <section>
            <div className="sharedSection">
                <div className="sectioncontent">
                   
                    <div>
                        <div className="row">
                          
                            <div className="col-lg-8 timeliner cv">
                                <img src="/static/img/ego-new.png" className="cv-img" alt="Rickard Magnusson"/>
                                <br/><br/>
                                <h1>Senior Software Engineer</h1>
                                <br/><br/>
                                <h2>PROFESSIONAL PROFILE</h2>
                                
                                <h4>A creative developer with 15 year’s experience in a variety of development. Resourceful due to my long experience in various employments. A good understanding of client requirements with good communication skills.  Likes to be challenged at work, to continue learn development. Have a good eye for details and is very curious to explore new things.</h4>
                                                             
                                <br /><br/>                                               
                                <h3>WORK EXPERIENCE</h3><br /><br /> 

                                <b>Role:</b> Senior Software Engineer, Capgemini AS Norge (Eika bank)<br />
                                <b>Responsibilities:</b> Works as a development consultant for Capgemini’s  customers in Norway. Tasks such as develop and export existing onprem programs to Azure. Responsibillity for the client customer register at E.I.K.A Banks AS. <br /><br /><em>Dynamics 365 CRM, C#, ASP.NET, MVC, .NET Core 2 (API-2), Azure, AutoFac, NHibernate, Servicebus, Eventgrid, Keyvault, Azure functions etc. Windows Forms applications</em>
                                <br /><br /> 
                                <b>Role:</b> Development/Market Director at Multibutiken AB, Sweden<br />
                                <b>Responsibilities:</b> Internal application development.  Develop a new profile with focus on digital campaigns and strategic  marketing.  Select and develop in a new e-commerce platform.<br /><br /> <em>C#, ASP.NET, MVC, ASP.NET Core 2, Razor, nHibernate, Fluent, Linq, SaaS, Mysql, SqlServer, Azure, hMail, AutoFac, After Effects</em>
                                <br /><br /> 
                                <b>Role:</b> Sharepont Developer at Santander Consumer bank AS, Norway<br />
                                <b>Responsibilities:</b> Developing and maintaining the company’s sites in SharePoint.  Focus on reducing deploymentstimes and increase the  performance of applications.  24/7 responsebility for www.santanderconsumer.no <br /><br /><em>C#, ASP.NET, MVC, WebForms, Razor, SqlServer, Azure, TFS, Poweshell, ServiceStack, Bootstrap, Backbone.  Sharepoint 2013, Windows Server 2014</em>
                                <br /><br /> 
                                <b>Role:</b> Developer/Market Director Assistent, New Wave AB, Sweden<br />
                                <b>Responsibilities:</b> Development of a new  intranet for resellers. Digital campaigns and strategic marketing. for both internet and  digital marketing. Development of app, and microsites.<br /><br /> <em>C#, ASP.NET, MVC, WebForms, MySql, Media Queries, Typo 3, Indesign, Photoshop, Illustrator</em>
                                <br /><br /> 

                                <b>Role:</b> Developer at Enterprisesoft, Sweden <br />
                                 <b>Responsibilities:</b> Manufacturer of applications and websites for  businesses.  Development of CMS system. Web design and manufacturing  of digital content for web and print. Interaction with customers worldwide.
C#, ASP.NET, MVC, WebForms, MySql, Media Queries, Indesign, Photoshop, Illustrator 
                              <br /><br /> 
                                <b>Role:</b> Developer at Medio Web Production, Sweden  <br />
                                 <b>Responsibilities:</b> Programming, web design and manufacturing of digital content for both Internet and print. Programming of CMS system. C#, ASP.NET, WebForms, MySql, Media Queries, Typo 3 Photoshop, Illustrator


                            </div>
                            <div className="col-lg-4 cv-side">
                               <br/>
                                <h3>CONTACT INFORMATION</h3>
                                <p>
                                Address: Nordanvindsv 2, H1203, <br/>
                                45160 Uddevalla  <br />
                                Date of birth: 690130  <br />
                                Phone: +46(0)76 881 80 23  <br />
                                Email: rickard.magnusson.9@gmail.com  <br />
                                LinkedIn profile:<br />
                                https://www.linkedin.com/in/bootproject <br />
                                Website: https://www.bootproject.no <br />
                                <br /><br />
                               </p>
                                <h3>EDUCATION</h3> 
                                <p>
                                    Certified developer at CIC (Ementor) 100p <br/>
                                    (object orientation, Java object-oriented  programming, System development.  <br />
                                    Updating my knowledge in mathematics, Swedish, English, Computers, etc. Komvux.<br />  
                                    Mechanical Engineering Education 3 years Secondary, School GullmarSchool  <br />
                                    Lysekil Elementary school at GullmarSchool in Lysekil Sweden<br /><br />
                                </p>
                                <h3>PROFESSIONAL SKILLS</h3>
                                <p>
                                    C# (Main language)<br /> 
                                    ASP.NET<br />
                                    .NET CORE -2.2 , V3 in progress <br />
                                    ASP.NET, <br />
                                    MVC, <br />
                                    RAZOR, <br />
                                    Webforms,  <br />
                                    EF, nHibernate Web Services, WebApi,  <br />
                                    Servicestack Autofac, DI, SaaS  <br />
                                    Html, jQuery, SASS/LESS, CSS <br />
                                    React JS, Next JS, Redux<br />
                                    IIS, SQL Server -2018, MySQL -6 <br />
                                    Sharepoint 2013, Dynamics 365 (CRM) 
                                    <br />Powershell,<br /> 
                                    TFS, Git, NuGet  <br />
                                    Windows Server - All Visual studio -2019 <br />
                                    Azure, ServiceBus, Azure functions, EventGrid <br />
                                    Photoshop, Ilustrator, Indesign, After Effects
                                    </p>

                                <h3>LANGUAGES</h3>
                                <p>
                                    Swedish: spoken, written   <br/>
                                    English - spoken, written <br/> 
                                    Portuguese spoken, written <br/>
                                    Norweigian spoken  
                                </p>
                                <h3>LICENSES</h3> 
                                <p>Holds MSDN Enterprise and have access and licenses for development tools like  Visual Studio, Windows Server, SQL Server and Azure. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
);

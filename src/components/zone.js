import React from "react";
import Content from "./content";
import Timeline from "./timeline";
import Search from "./search";

/*
 * A conversion list of modules in application
 */
const Modules = {
    Content: Content,
    Timeline: Timeline,
    Search: Search
}; 

/*
 * Zone component
 * Create components based on configuration.
 */
class Zone extends React.Component {

    /*
     * Get params from store
     * @param {string} name, the zone name
     * @param {object} current, the page requested
     * @param {object} store, the redux store data
     */
    constructor({ name, current, store }) {
        super();

        const data = store.getState();
        
        this.state = {
            zone: name,
            page: current,
            data: data
        };
    }

    /*
     * Renders a React element
     * @param {object} component
     * @param {object} data
     */
    renderElement(component, data) {
        var key = "key-" + Math.random(1000);
        return React.createElement(
            Modules[component],{
                key: key,
                id: key,
                props: data
                
            }
        );
    }

    /*
     * Recurse elements
     * @param {object} module
     * @param {object} data
     */
    recurseElements(module, data) {
        return this.renderElement(module, data);
    }

    /*
    * Get data for a components
    * @param {object} component
    */
    getZoneDataForPage(component) {

        let elements = [];
        var components = this.state.data[component.toLowerCase()];

        if (!components) { //If component does not any contain data.
            elements.push(component);
        } else {
            if (Object.prototype.toString.call(components) === '[object Array]') {
                components.map(c => {
                    if (c.zone === this.state.zone && c.path === this.state.page.path) 
                        elements.push(c);
                });

            } else {
                if (components.zone === this.state.zone && components.path === this.state.page.path)
                    elements.push(components);
            }
        }
        
        return elements;
    }

    /*
     * Creates modules for each zone.
     * @param {Array} zones
     */
    createZoneElements(zones) {

        let elements = [];
        for (var item in zones){
            if (zones[item].name === this.state.zone){
                const modules = zones[item].modules.map(m => m);

                for (var component in modules){
                    var moduledata = this.getZoneDataForPage(modules[component]);
                    for (var module in moduledata) {
                        if(moduledata[module].includeProps)
                            elements.push(this.recurseElements(modules[component], this.state.data)); //Include all props (For for search)
                        else
                            elements.push(this.recurseElements(modules[component], moduledata[module]));
                    } 
                }
            }
        }
       return elements;
    }

    render() {
        const { page } = this.state;

        /** Render zones*/
        var elements = this.createZoneElements(page.zones);

        var unique = "key-" + Math.random(1000);
        return React.createElement("div", { key: unique,  props: this.props }, elements);
    }
}
export default Zone;
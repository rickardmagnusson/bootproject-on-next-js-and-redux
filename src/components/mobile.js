﻿/* eslint-disable */
import React from "react";

/*
 * Top navigation, also handle mobile navigation
 */
export default class MobileNav extends React.PureComponent {

    constructor(props) {
        super();

        this.state = {
            routes: props.routes,
            route: props.route
        }
    }

    getClass(page) {
        return page.path.split("/")[1] === this.props.route.split("/")[1] ? "nav-item active" : "nav-item none";
    }

    render() {

        const { routes } = this.state;

        return (
            <nav className="navbar navbar-expand-lg navbar-light top">
                <div className="d-flex flex-grow-1">
                    <a className="navbar-brand" href="/"><img src="/static/img/logo.svg" alt="Logo" className="brand" /> Bootproject</a>
                    <div className="w-100 text-right">
                        <button className="navbar-toggler" id="menuControlOpen">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </div>
                <div className="flex-grow-1 text-right overlay" id="nav">
                    <img src="/static/img/logo.svg" alt="Logo" className="overlay-icon" />
                    <div id="menuControlClose"><a>&times;</a></div>
                        <ul className="navbar-nav ml-auto flex-nowrap">
                        {routes.map(r => r.hidden===false &&
                                <li key={"li-" + r.path}>
                                    <a key={r.path} href={r.path} className={this.getClass(r)}>{r.title}</a>
                                </li>
                            )}
                        </ul>
                    <div className="version">Bootproject 2019</div>
                </div>
            </nav>
        );
    }
}
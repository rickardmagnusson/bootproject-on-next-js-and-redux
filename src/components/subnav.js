﻿/* eslint-disable */
import React from "react";
import Link from "next/link";

export default class SubNav extends React.Component {
    constructor(props) {
        super(props);
    }

    createOverlay() {
        return (
            <div className="sub timeliner"><h2>Projects</h2>
                {this.props.posts.map((t, i) => <div key={"pointer" + i} className='timeline-point'><div className='timeline-icon'></div>
                    <div key={"blocked-" + i} className='timeline-block'>
                        <Link key={t.id} href={"/projects/" + t.slug} ><a className={t.slug === this.props.slug ? "active" : "none"}>{t.title}</a></Link>
                    </div>
                </div>)}
            </div>
        );
    }

    render() {
        return (
            <>
                <div className="overlay-sub" id="overlay">
                    <div id="menuSubControlClose">&times;</div>
                    <div className="overlay-content-sub">
                        {this.createOverlay()}
                    </div>
                </div>
                <div className="overlay-sub-action">
                    <a className="btn" id="subnav">{this.props.title}</a>
                </div>
             </>
        );
    }
}
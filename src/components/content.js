/* eslint-disable */
import React from "react";
import SyntaxHighlighter from "react-syntax-highlighter";

/*
 * Content module.
 * Returns content or a content list,
 * if content contains a list.
 */
export default class Content extends React.Component{

    constructor({ props }) {
        super();

        this.state = { props };
    }

    render() {

        if (this.state.props.list.length > 0) {
            return (
                <div className="sectionroot">
                    <section>
                        <div className={"sharedSection" + this.state.props.style}>
                            <div className="sectioncontent">
                                <header><img src={this.state.props.icon} alt="multi" className="big" /> <h2>{this.state.props.title}</h2></header><br/>
                                
                                <ul className="shared_grid">
                                    {this.state.props.list.map((i,e) => <li key={e}><h2><img src={i.icon} alt="multi" className="small" /> {i.title}</h2>
                                        <p dangerouslySetInnerHTML={{ __html: i.content }} />
                                        <br /><br />
                                        {i.code && <SyntaxHighlighter language="javascript">{i.code}</SyntaxHighlighter>}
                                    </li>)}
                                    </ul>
                               
                            </div>
                        </div>
                    </section>
                </div>
            )
        } else {
            return (
                <div className={"sectionroot " + this.state.props.style}>
                    <section>
                        <div className="sharedSection">
                            <div className="sectioncontent">
                                <header><img src={this.state.props.icon} alt="multi" className="mini" /> <h2>{this.state.props.title}</h2></header>
                                <span dangerouslySetInnerHTML={{ __html: this.state.props.content }} />
                                <br /><br />
                                {this.state.props.code && <SyntaxHighlighter language="javascript">{this.state.props.code}</SyntaxHighlighter>}
                            </div>
                        </div>
                    </section>
                </div>
            )
        }
    }
}
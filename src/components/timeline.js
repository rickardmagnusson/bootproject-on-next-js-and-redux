/*eslint-disable*/
import React from 'react';

/*
 * Timeline module
 */
const Timeline = ({ props }) => {
    return (<section>
        <div className="sharedSection">
            <div className="sectioncontent">
                <div className="timeliner">
                    <header><h2>{props.title}</h2><div>{props.content}</div></header>
                    {props.list.map((p ,i)=>
                        <div key={"timline-" + i} className='timeline-point'>
                            <div key={"icon-" + i} className='timeline-icon'></div>
                            <div key={"block-" + i }className='timeline-block'>
                                <div key={"content-" + i} className='timeline-content'>
                                    <h3 key={"title-"+ i}>{p.title}</h3>
                                    <div key={"cnt-"+ i} dangerouslySetInnerHTML={{ __html: p.content }} />
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
    </section>);
};

export default Timeline;
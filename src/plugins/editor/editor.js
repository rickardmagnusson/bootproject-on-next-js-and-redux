﻿import React from "react";
import SyntaxHighlighter from "react-syntax-highlighter";

const Editor = (props) => {
    <SyntaxHighlighter language="javascript">{props.html}</SyntaxHighlighter>
};

export default Editor;
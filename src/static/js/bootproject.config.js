﻿/*
 * Bootproject scripts 2019
 * Author: Rickard Magnusson, Bootproject
 */

try {
    var nav = document.getElementById('nav');
    var openMenu = document.getElementById('menuControlOpen');
    var closeMenu = document.getElementById('menuControlClose');

    openMenu.addEventListener('click', function (e) {
        document.body.classList.toggle("stop-scrolling");
        nav.classList.toggle('menu-active');
    });

    closeMenu.addEventListener('click', function (e) {
        document.body.classList.toggle("stop-scrolling");
        nav.classList.toggle('menu-active');
    });



    var overlay = document.getElementById('overlay');
    var subnav = document.getElementById('subnav');
    var closeSubMenu = document.getElementById('menuSubControlClose');

    subnav.addEventListener('click', function (e) {
        document.body.classList.toggle("stop-scrolling");
        overlay.classList.toggle('menu-sub-active');
    });

    closeSubMenu.addEventListener('click', function (e) {
        document.body.classList.toggle("stop-scrolling");
        overlay.classList.toggle('menu-sub-active');
    });

} catch (e) {
    /**/
}
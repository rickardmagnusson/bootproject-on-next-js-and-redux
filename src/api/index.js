﻿
/*
 * API data
 */

const Service = {
    pages: [
        {
            id: 1,
            title: "Home",
            meta_title: "Bootproject, Senior Software Engineer",
            path: "/",
            meta_description: "Senior Software Engineer developing test suites. Test and implementation of new features in node, .NET, Core, React, Redux and other development.",
            meta_keywords: "Software,engineer,react js,next js,redux,multitenancy.net core,azure,kubernetes",
            layout: "/index",
            template: "template.home",
            hidden: false,
            zones: [
                {
                    name: "BeforeContent",
                    modules: ["Content"]
                },
                {
                    name: "Content",
                    modules: ["Content", "Timeline"]
                },
                {
                    name: "AfterContent",
                    modules: ["Content"]
                }]
        },
        {
            id: 2,
            title: "Projects",
            meta_title: "Projects on Bootproject",
            path: "/projects",
            hasSubroutes: true, /*Menu helper*/
            meta_description: "Bootproject projects descriptions",
            layout: "/projects",
            template: "template.projects",
            hidden: false,
            zones: [
                {
                    name: "Content",
                    modules: ["Project"]
                }]
        },
        {
            id: 6,
            title: "Search",
            meta_title: "Search on Bootproject",
            path: "/search",
            meta_description: "Bootproject projects search",
            layout: "/index",
            template: "template.home",
            includeProps: true,
            hidden: false,
            zones: [
                {
                    name: "BeforeContent",
                    modules: ["Search"]
                }]
        }
        ,
        {
            id: 3,
            title: "Core",
            meta_title: "Multitenancy .Net Core 2.2",
            path: "/core",
            meta_description: "Bootproject multitenancy in .Net core 2.2",
            layout: "/index",
            template: "template.home",
            hidden: false,
            zones: [
                {
                    name: "BeforeContent",
                    modules: ["Content"]
                }]
        },
        {
            id: 4,
            title: "About",
            meta_title: "About Bootproject",
            path: "/about",
            meta_description: "About Bootproject",
            layout: "/index",
            template: "template.home",
            hidden: false,
            zones: [
                {
                    name: "BeforeContent",
                    modules: ["Content"]
                }]
        },
        {
            id: 5,
            title: "Contact",
            meta_title: "Contact Bootproject",
            path: "/contact",
            meta_description: "Contact Bootproject",
            layout: "/index",
            template: "template.home",
            hidden: false,
            zones: [
                {
                    name: "BeforeContent",
                    modules: ["Content"]
                }]
        },
        {
            id: 11,
            title: "CV",
            meta_title: "Bootproject cv",
            path: "/cv",
            meta_description: "Bootproject cv",
            layout: "/cv",
            template: "template.home",
            hidden: true,
            zones: [
                {
                    name: "Content",
                    modules: ["Content"]
                }]
        }
    ],

    search: {
        path: "/search",
        zone: "BeforeContent",
        includeProps: true
    },

    //content on pages
    content: [
        {
            id: "1",
            path: "/",
            zone: "Content",
            style: "",
            title: "Development",
            content: "",
            icon: "/static/img/layers.svg",
            list: [
                {
                    icon: "/static/img/layers.svg",
                    title: "Multitenancy V2",
                    content: ".NET Core 2.2 package that makes ease of using multitenancy and fast deployments of new applications. <a target='_blank' href='https://www.nuget.org/packages/Bootproject.Multitenancy.Core/' class='contentlink'>Download at NuGet</a>"
                },
                {
                    icon: "/static/img/bitbucket.png",
                    title: "Bitbucket",
                    content: "Bitbucket is a git service to share / store your code, but also for deployments for eg. to Azure etc, thru their piplines."
                },
                {
                    icon: "/static/img/react.png",
                    title: "React Platform",
                    content: "React with Redux and Next js is a really cool platforms to make advanced static websites, in an easy way."
                }
            ]
        }
        ,
        {
            id: "2",
            path: "/",
            zone: "Content",
            style: " mobile",
            title: "React Zones",
            content: "Current project on React, Redux and Next Js contains an implementation of creating Zones. A zone is a component that is defined in a template, then filled from a service API. When you define templates you just enter what zones to get React Components from. No matter what the zone name is, as long as you define it in the module This website is built with the zone layout. Keeping it simple and generic is the way we like it.",
            code: "<Zone name=´Content´ store={this.props.store} current={this.props.current}/>",
            icon: "/static/img/code.svg",
            list: []
        },
        {
            id: "3",
            path: "/",
            zone: "AfterContent",
            style: " mobile",
            title: "Performance",
            content: "Performance is indeed an important thing when it comes to websites. Building website with techniques, that displays the page fast. This site doens't event require any cache. This project is about to create a complete application for websites/cellphones. Latest addition is the Redux implementation speeds up the site load and servs the components with properties fast, in a stateful way also render the content inside modules/components.",
            icon: "/static/img/services.png",
            list: []
        },
        {
            id: "4",
            path: "/",
            zone: "AfterContent",
            style: "",
            title: "About",
            content: "Who? Me, Rickard at Bootproject, currently employeed as a consultant in Norway. I have been in a lot of projects in the past. Latest project is about moving onprem services  to Azure. Taking windows services and move them to Azure functions, and also create a new businessrules component to transport data to Dynamics 365 CRM.",
            icon: "/static/img/user.png",
            list: []
        }
        ,
        {
            id: "5",
            path: "/projects",
            zone: "Content",
            style: "",
            title: "Projects",
            content: "Current project (Learing React) contains an implementation of creating Zones. A zone is a component that is defined in a template, then filled from a service API. When you define templates you just enter what zones to get React Components from. This website is built with the zone layout.Keeping it simple and generic is the way I like it.",
            icon: "/static/img/services.png",
            list: []
        }
        ,
        {
            id: "6",
            path: "/contact",
            zone: "BeforeContent",
            style: "",
            title: "Contact",
            content: "If you feel a need to contact Bootproject, you can send an email to the address in the footer. <br/> //Cheers",
            icon: "/static/img/contact.svg",
            list: []
        }
        ,
        {
            id: "9",
            path: "/about",
            zone: "BeforeContent",
            style: "",
            title: "About",
            content: "<br/><h2>A creative fullstack Software Engineer with over 15 year’s experience in a variety of development. Resourceful due to my long experience in various employments. A good understanding of client requirements with good communication skills. Likes to be challenged at work, to continue learn development. Have a good eye for details and is very curious to explore and learn new things.</h2> <br/><h2>Who?</h2> Me, Rickard at Bootproject, started of in Java in the early 2000, and later on I continued with C# in Visual Studio. I have been in a lot of various projects in the past. Im a very dynamic developer that has good communication skills. Worked in environments where all my knowledge and experience was required. I also gained a lot of experience from my proprietary CMS, multitenancy projects, and right in this moment also React, Next Js and Redux. Professional skills like C#, ASP - MVC, Azure, Sharepoint, Dynamics CRM, SQL Server, developing in front as backend. <br/><br/><h2>Design, marketing</h2>Design, from simple websites to complex layouts with xml generation of documents or catalogs in Indesign, Illustrator and Photoshop. Adobe animate for 3D designs. Have a good eye when it comes to advertising materials, catalogs, banners, folders.<br/><br/>I built this site to share and for others may see the variety of projects during my employments.<br/><br/><b>Im currently looking a position in Lisbon Portugal, so if you are a recuiter or need a Senior Fullstack Software Engineer, please contact me at bootproject@icloud.com</b><br/><br/>Have a look at <a href='/cv' class='contentlink'>my CV</a> and you'll see my long experience in various projects. Here's my <a href='https://www.linkedin.com/in/bootproject/' target='_blank' class='contentlink'>LinkedIn profile</a>.<br/><em>For the full CV, please contact me and I'll send it to you.</em>",
            icon: "/static/img/user.png",
            list: []
        },
        {
            id: "7",
            path: "/blog",
            zone: "Content",
            style: "",
            title: "Yet to be developed",
            content: "Next module to be created is a blog. A was thinking about create a blog with links, -Tags and content dependent on that tags.",
            icon: "/static/img/services.png",
            list: []
        }
        ,
        {
            id: "8",
            path: "/projects",
            zone: "BeforeContent",
            style: "",
            title: "Projects",
            content: "Current project (Learing React) contains an implementation of creating Zones. A zone is a component that is defined in a template, then filled from a service API. When you define templates you just enter what zones to get React Components from. This website is built with the zone layout. Keeping it simple and generic is the way we like it.",
            icon: "/static/img/services.png",
            list: []
        }
        ,
        {
            id: "10",
            path: "/core",
            zone: "BeforeContent",
            style: "",
            title: "Multitenancy in .Net Core 2.2",
            content: "This project is an extension of nHibernate's ISessionFactory and makes it incredible easy to use multiple sessions within the same application. It's used to define different domains and their databases. Uses MySql as database. (Download source to change to your favorite database)<br/><br/>Since the previous versions in .Net 4.6 this is more lightweighted, without lack of functionallity. Built in model creation. When adding a new model or change the entity, it's automatically gets replicated in database. <br/><br/>I started this project a couple years ago when I needed a fast way of creating websites and applications. In just a few lines of code with this project, I can make a solution ready to be modeled in a few seconds.<br/><br/><pre class='codeblock'>pm> Install-Package Bootproject.Multitenancy.Core -Version 2.1.0</pre><br/><a class='contentLink source' href='https://www.nuget.org/packages/Bootproject.Multitenancy.Core/' target='_blank'>Download at Nuget</a>",
            icon: "/static/img/services.png",
            code: `ConfigureServices: \nservices.AddMultitenancy(); \napp.UseMultitenancy(); 

Add "DomainConfiguration" section to appsettings.json like the example below:\n
    "DomainConfiguration": {
        "Domains": [
                {
                    "Name": "bootproject",
                    "Dns": "localhost,bootproject.net",
                    "ConnectionString": "server=localhost;port=3306;database=test1;uid=root;password=1234"
                },
                {
                    "Name": "example",
                    "Dns": "example.com",
                    "ConnectionString": "server=localhost;port=3306;database=test2;uid=root;password=1234"
                }
            ]
        }`,
            list: []
        }
    ],

    //Projects
    projects: {
        posts: [
            {
                slug: "build-a-cms-with-sanity",
                title: "Sanity CMS",
                content: "<h2>From the previous article, I wrote some words about the challenge when it comes to build a static but dynamic site. Ultimate static site, continued. <a href='/projects/nuxt-vue-static-site/' class='contentlink'>Read first part</a></h2><img src='/static/img/sanity.png' class='contentimg' /><br/><br/><img class='icon-fluid img-fluid float-left' src='/static/img/sanitylogo.png' /> Since I use Next JS to export or deploy as I prefeer to call it, it's a bit challenging when it comes to the part of creating content and run the deployment on API changes. I do think that I have a solution that will suit my need for this. I remember that I tested the Sanity Headless CMS in the past. This time I would actually get it a go. Sanity is a Norwegian based company and is very interresting and cool because of the services they provide. I use their Headless CMS. They was also the reason why I started to build applications with React and Next JS. When I noticed the performance, I got cuirous how it was made. Now I know! <a target='_blank' class='contentlink' href='https://sanity.io'>Visit Santity</a><br/><br/><h2>Sanity platform</h2>The only thing I need is to put content in Sanity (CMS), then load all data from the api into my application to rebuild my site with all modules, zones ect.<br/><br/><h2>Middleware</h2>By creating a middlware to load all API data from the Sanity API(GROQ) into my application, it gets automatically rebuilt on changes.<br/><br/><h2>Install Sanity</h2>This part is the best. In one line you are up and running, except for the scheme that you need to create to make your fields.<br/><br/><pre class='codeblock'>npm install -g @sanity/cli</pre><br/><br/>Sanity is also very intelligent, just by adding a schema, to build up the content structure in Sanity, it directly becomes editable. It's so easy to learn and use. See the schema for my site below. It's not complete, it still need some tuning, like initialValues and some other fields. But I think you get the idea.",
                meta_title: "How to build a content management system with Sanity",
                meta_description: "In this part I going to describe how I made this site editable.",
                layout: "/projects",
                template: "template.projects",
                created: "2019-11-03",
                summary: "<h2>Summary</h2>By adding a Santity platform client API, I still have a static site, but in this way, it is also dynamic. I dont even  have to use a database. The conslusion is that the whole site is very cheap to run, and it also run with minimal server resources.",
                code: `
export default createSchema({

    name: 'API',

    types: schemaTypes.concat(
        [
            {
                title: "Page",
                name: "page",
                description: "Page",
                type: "document",    
                icon: FaFileAlt,
                fields: [
                    {
                        title: "Title",
                        name: "title",
                        subtitle: "Title of page",
                        description: "Page title",
                        type: "string"
                    },
                    {
                        name: "published",
                        title: "Published",
                        type: "boolean",
                        initialValue: "true"
                    },
                    {
                        title: "Url",
                        name: "url",
                        type: "string",
                        description: "Page url"

                    },
                    {
                        name: "sortorder",
                        type: "number",
                        title: "Sort order",
                        description: "Sort order of page"
                    },
                    {
                        title: "Meta description",
                        name: "meta_description",
                        type: "string",
                        description: "Page description"

                    },
                    {
                        title: "Meta keywords",
                        name: "meta_keywords",
                        type: "string",
                        description: "Page keywords"

                    },
                    {
                        title: "Layout",
                        name: "layout",
                        type: 'array',
                        of: [{ type: 'string' }],
                        description: "Select a layout for the page.",
                        options: {
                            list: [
                                { value: '/index', title: 'Default' },
                                { value: '/projects', title: 'Project' },
                                { value: '/cv', title: 'CV layout' }
                            ]
                        }
                    },
                    {
                        name: 'zones',
                        type: 'array',
                        title: 'Zones',
                        description: "Add a zone and modules to that zone",
                        of: [{ type: 'zones' }]
                    }
                ],
                orderings: [{
                    title: 'Sort by',
                    name: 'sortby',
                    by: [
                        { field: 'sortorder', direction: 'desc' }
                    ]
                }],
                preview: {
                    select: {
                        title: 'title',
                        published: 'published',
                        description: "meta_keywords"
                    },
                    prepare(selection) {
                        const { title, published, description } = selection;
                        return {
                            title: title,
                            subtitle: published === true ? "Published" : "Unpublished",
                            description: description
                        };
                    }
                }
            },
            {
                name: "content",
                title: "Content",
                type: "document",
                description: "Page content field",
                fields: [
                    {
                        type: "string",
                        name: "title",
                        title: "Title",
                        description: "Internal"
                    },
                    {
                        title: 'Text',
                        name: 'content',
                        type: 'array',
                        of: [{ type: 'block' }]
                    },
                    {
                        name: 'Zones',
                        description: "Add this content to zone:",
                        type: 'array',
                        title: 'Zones',
                        of: [{ type: 'zones' }]
                       
                    },
                    {
                        name: "path",
                        title: "Page",
                        description: "Display on page:",
                        type: "array",
                        of: [
                            {
                                type: "reference",
                                to: [{ type: "page" }]
                            }
                        ]
                    }
                ]
            }
            ,
            {
                name: "timeline",
                type: "document",
                title: "Timeline",
                icon: FaBusinessTime,
                description: "Timeline item",
                layout: "/projects",
                fields: [
                    {
                        type: "array",
                        name: "posts",
                        title: "Posts",
                        description: "List of posts",
                        of: [
                            {
                                type: 'post'
                            }
                        ]
                    }
                ]
            },
            {
                name: "post",
                type: "object",
                title: "Post",
                icon: MdComment,
                fields: [
                    {
                        type: "string",
                        name: "title",
                        title: "Title",
                        description: "Internal"
                    },
                    {
                        type: "string",
                        name: "slug",
                        title: "Slug",
                        description: "Url slug"
                    }
                    ,
                    {
                        type: 'array', 
                        name: "content",
                        title: "Content",
                        of: [{ type: 'block' }]
                    }
                    ,
                    {
                        type: "code",
                        name: "Code",
                        title: "Code",
                        description: "Project code"
                    },
                    {
                        name: 'publishedAt',
                        type: 'datetime',
                        title: 'Published at',
                        description: "Publish time"
                    }
                    ,
                    {
                        type: 'array',
                        name: "summry",
                        title: "Summary",
                        of: [{ type: 'block' }]
                    }
                ]
            },
            {
                name: "zones",
                title: "Zone",
                type: "object",
                fields: [
                    {
                        name: "name",
                        type: "array",
                        of: [{ type: 'string' }],
                        initialValue: "Content",
                        options: {
                            list: [
                                { value: 'Header', title: 'Header' },
                                { value: 'Content', title: 'Content' },
                                { value: 'BeforeContent', title: 'BeforeContent' },
                                { value: 'AfterContent', title: 'AfterContent' },
                                { value: 'SidebarRight', title: 'Sidebar right' },
                                { value: 'Footer', title: 'Footer' }
                            ]
                        }
                    },
                    {
                        name: 'modules',
                        description: "Use these modules in this zone",
                        type: 'array',
                        title: 'Modules',
                        of: [{ type: 'string' }],
                        options: {
                            list: [
                                { value: 'Content', title: 'Content' },
                                { value: 'Timeline', title: 'Timeline' }
                            ]
                        }
                    }
                ]
            }
        ])
});
`
            }
            ,
            {
                slug: "business-rules-csharp-part-2",
                title: "Business rules continued",
                content: "<h2>How to create a Rule based system in c sharp? Below is an image that descibes the rules we want to apply. If a rule has a match, continue to next rule until we meet the expected result. If you haven't read the first part, <a class='contentlink' href='/projects/business-rules-csharp/'>read it here</a></h2> <br/><h4>Rules</h4><img src='/static/img/rules.desc.png' class='contentimg'/> <br/><br/><h2>Simplicity!</h2>How do we make our Business Rules understandable for non developers? Can they also be a part of the discussion, and also be able to explain for the client what's going on. -I sure think so. Let me explain how in the next section.<br/><br/><h2>What is a Business Rule?</h2>When I started this project I searched the Internet for what types of Business rules there is. There's XML based, programatical etc. A lot of different techniques to achieve the same result. I desided to go for programatical, since I already have functions for it. A businessrule should describe what to reach and todo with the result. If you see the image, which we developers offens get sent to us. It describes what functions needed. Also what we should do with items that reach a certain level. <br/><br/>If we can make all functions with the same name, it's also easy to follow. We know what we need to return.<br/><br/>Basically a Rule is just a function. By adding the Composite Rule, we can now combine these functions in a Fluent way!! I like that, because it is so easy to read and understand. For eg. <pre class='codeblock'>var AllowEditContent = IsEditor.And(IsPartOfGroupAdmin).Or(IsPartOfGroupPowerUsers);</pre> It's understandable for anyone, isn't it?<br/><br/>I dont know if this is the best solution, it's just my way of solve a problem and make it easier to read and follow all business rules. Because when a system get's bigger and more complicated, we need to have an easy way to implement new functions, and be understandable for anyone. In this way we can test any single rule before we proceed that will guarantee the result.<br/><br/>If you prepare all rules, you can actually pre-make all Rules, to match all rules required for you solution.<br/><br/>As a result I created a small console app, for you to study. <br/>See the code below, or checkout the code on <a target='_blank' href='https://bitbucket.org/rickardmagnusson/business-rules/src/master/BUManager/' class='contentlink'>Bitbucket</a>",
                meta_title: "How to build a Business rules management system",
                meta_description: "Build a Business Rules system in c-charp part two.",
                layout: "/projects",
                template: "template.projects",
                created: "2019-10-29",
                summary: "<h2>Summary</h2>It sounds complicated for some people when I mention Business Rules. But, if we can explain it by show a Rule on a picture or scheme, and then review the code, it will all be much clearer. I hope you enjoyed my writing. Cheers!",
                code: `
    public static partial class RuleProcessor
    {
        //Print status colored
        static Action<string> P = (s) => { Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine(s); };
        static Action<string> F = (s) => { Console.ForegroundColor = ConsoleColor.Yellow; Console.WriteLine(s); };
        static Dictionary<RequestType, List<RegisterEvent>> Events = new Dictionary<RequestType, List<RegisterEvent>>();

        public static void Process(List<RegisterEvent> items)
        {
            items.ForEach(c=> ProcessItem(c));
            PrintDict();
        }


        private static void ProcessItem(RegisterEvent item)
        {
            // PreRun all Rules for the Event
            var validateIsBank = ValidateSource(item);
            var validateIsActive = ValidateStatus(item);
            var validateInsuranceType = IsInsuranceType(item);


            /* 
             * Now we can group these rules to meet a specific requirement.
             * Group several rules to match a specific item.
             * What we want to achieve, get's more clear now.
             */
             var ShouldPassActiveAndIsBank =
                validateIsBank
                .And(validateIsActive);

             /*
             * The name "ShouldPassActiveAndIsBank" can later on, 
             * be used to run the event in a specific function 
             * or store the event in a table in a database. 
             * We now know what todo with this item.
             * 
             * The image(schema) describes what todo with an item that reaches this level.  
             */

            if (ShouldPassActiveAndIsBank.PassedItem){
                AddToDict(RequestType.IsActiveAndIsBank, item);
            }
            else {
                var ShouldBeInsuranceType = validateInsuranceType;

                if (ShouldBeInsuranceType.PassedItem)
                    AddToDict(RequestType.IsInsurance, item);
                else
                    AddToDict(RequestType.UnKnownType, item);
            }
        }

        #region Helpers

        private static void PrintDict()
        {
            foreach(var key in Events.Keys)
            {
                var list = Events[key];
                foreach (var evt in list)
                    if(key==RequestType.UnKnownType)
                        F($"Unknown type: {evt.Name}");
                    else
                        P($"Item found: {key} : {evt.Name}");
            }
        }

        private static void AddToDict(RequestType key, RegisterEvent item)
        {
            var items = new List<RegisterEvent>();

            if (!Events.ContainsKey(key)) {
                items.Add(item);
                Events.Add(key, items);
            }
            else
            {
                Events.TryGetValue(key, out items);
                items.Add(item);
            }
        }

        #endregion
    }
`
            }
            ,
            {
                slug: "business-rules-csharp",
                title: "Business rules",
                content: "<h2>Build a Business Rules system in c-charp. Pass or not to pass. Below is an image that descibes rules. If a rule has a match, continue to next rule until we meet the expected result.</h2> <br/><h4>Rules</h4><img src='/static/img/rules.svg' class='contentimg'/> <br/><br/><h2>Introduction, background</h2>When I was started at a bank in Norway, I was left a lone in a huge project where I needed to take care of the client register. At a first look, it looked complete, but the problem started when I was diving deeper into the code. Such as functions in functions that didn't end anywhere. At least not to be readble. I needed to develop most of the code in a new more Solid, and Clean code way.<br/><br/><h2>Business rules</h2>I started of by search the internet for such a system, but not a single one meet the requirements for my need. I needed something new. The main thought, was to create each rule as a function, with the ability to combine these when I run it.<br/><br/>It's running each item in a list thru these rules, it returns back one list of valid items and one list of not passed item. By combining the rules for a specific need(Rules) we get a list of Valid(passed) items. A rule just deside if it has a match. See code below. <br/><br/> And Yeah, now we actually have a named rule-system containing our Business Rules. It's easy to manage new rules and change the order and the expected result as we like, rather than before where we needed to go to a bunch of functions to achieve the result. It was also hard to change.<br/><br/><h2>Future</h2>In the future I think that I will change the rules to only apply to one item, rather then 'create a list of results'. Then it should become even more easy to adjust.",
                meta_title: "How to build a Business rules management system",
                meta_description: "Build a Business Rules system in c-charp.",
                layout: "/projects",
                template: "template.projects",
                created: "2019-10-28",
                summary: "<a href='/projects/business-rules-csharp-part-2/' class='contentlink'>Continue read the second part</a>",
                code: `

        //Combining the business rules to let it be more readble.
        var foundcustomers = 
        BankCrmEqualsBankKr(list)
        .And(ValidateSSNEqualsToKr(list))
        .And(IsActive(list))
        .Or(InActive(list))
        .And(ValidateHasOtherSources(list))
        .And(BecomeCustomerStatus(list));




        //The rules
        /// <summary>
        /// This is the first rule to run. The ActivityStatus needs to be of new type.
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> RunRulesForCustomer =
            list => new RunRulesForCustomer<KundeRegisterEvent>(list, list.ShouldRunRulesForCustomer());


        /// <summary>
        /// Source = Kerne
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateSource =
            list => new SourceRule<KundeRegisterEvent>(list, ValidSourceList);



    //Base rules: Can be AND, OR, NOT or a Composite Rule

    /// <summary>
    /// Base class for a Rule
    /// </summary>
    /// <typeparam name="T">The type to run a statment on</typeparam>
    public abstract class CompositeRule<T> : IRule<T>
    {
        private List<T> targets = new List<T>();

        public CompositeRule(List<T> source)
        {
            this.targets = source;
        }

        /// <summary>
        /// Make sure to set base in class : base(source)
        /// Important that source is defined becuase this generates the Passed / NotPassed items
        /// </summary>
        /// <param name="source"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        public CompositeRule(List<T> source, IRule<T> left, IRule<T> right)
        {
            this.targets = source;
        }


        /// <summary>
        /// Business rule execution
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public abstract bool IsSatisfiedBy(T o);


        /// <summary>
        /// Return a list of items that not passed validation.
        /// </summary>
        public List<T> NotPassedItems
        {
            get { return targets.Where(c => !IsSatisfiedBy(c)).ToList(); }
        }


        /// <summary>
        /// Return a list of items that passed validation.
        /// </summary>
        public List<T> PassedItems
        {
            get { return targets.Where(c => IsSatisfiedBy(c)).ToList(); }
        }

        /// <summary>
        /// Add an And statement to Group.
        /// </summary>
        /// <param name="Rule"></param>
        /// <returns></returns>
        public CompositeRule<T> And(IRule<T> Rule) 
        {
            return new AndRule<T>(targets, this, Rule);
        }


        /// <summary>
        /// Add an Or statement to Group
        /// </summary>
        /// <param name="Rule"></param>
        /// <returns></returns>
        public CompositeRule<T> Or(IRule<T> Rule)
        {
            return new OrRule<T>(targets, this, Rule);
        }


        /// <summary>
        /// Add an Not statement to Rule.
        /// </summary>
        /// <param name="Rule"></param>
        /// <returns></returns>
        public CompositeRule<T> Not(IRule<T> Rule)
        {
            return new NotRule<T>(targets, Rule);
        }
    }




    //Applies to (AVVENTER_SJEKK, AKTIV_RELASJON, AKTIV)
    public class ActivityStatusRule<T> : CompositeRule<T>
    {
        List<string> targets;

        public ActivityStatusRule(List<T> source, List<string> targets) : base(source)
        {
            this.targets = targets;
        }

        //Businessrule
        public override bool IsSatisfiedBy(T o)
        {

          dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent)) //Maybe make this more generic
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");

            if (targets.Contains(item.Status.ToString()))
                return true;
            return false;
        }
    }
`
            },
            {
                slug: "norwegian-ssn-generator",
                title: "Generate a valid Norwegian SSN",
                content: "I was facing a test project where I needed to generate test customers for the client in C#. Since I'm lazy and like to create dynamic functions, I needed to create a function that could return a customer with a valid, but though fake SSN(Social security number), the gender, a fore and lastname. I started to search Google, there must be someone who actually already accomplished this function, - Ohh, none had acheived this yet. I needed to create this function on my own, because I really wanted to make the testing for my customer as easy as possible. <br/><br/><h2>Event Manager</h2>Below is a screenshot of the event manager that creates the event and send the event as a JSON to create a new record in Dynamics CRM with the business rules applied.<br/><br/><img src='/static/img/eventmanager.png' class='contentimg' alt='Eventmanager'/><br/><br/><h2>Generate SSN</h2> See the code below. Full code is located at my space on Bitbucket. <br/><br/>I dicovered that there was a function in a Wiki, that explains how to calculate if its a valid SSN. Hum, I needed to reverse the function, and this is the code that I came up with.<br/> <br/><a target='_blank' class='contentLink' href='https://bitbucket.org/rickardmagnusson/azure.functions.eventprocessor/src/develop/Consumer/Extensions/Extensions.cs'>For full source click here</a>",
                meta_title: "How to generate a valid but fake Norwegian SSN",
                meta_description: "How to generate a valid but fake Norwegian SSN with name and gender.",
                layout: "/projects",
                template: "template.projects",
                created: "2019-10-19",
                code: `
public static partial class Extensions
    {
        /// <summary>
        /// Function to create a fake but valid Norwegian SSN
        /// </summary>
        /// <returns>A valid SSN object with SSN number, name, lastname and gender</returns>
        public static SSN GenSSN()
        {
            //Random date
            Func<DateTime> RandomDateFunc()
            {
                var start = new DateTime(1900, 1, 1); // Little bit optmistic by year, anyhow... = 119 years old!!
                var gen = new Random();
                int range = ((TimeSpan)(DateTime.Today - start)).Days;
                return () => start.AddDays(gen.Next(range));
            }

            var rex = new Regex(@"^(?<birthdate>(?<day>\d\d)(?<month>\d\d)(?<year>\d\d))(?<individual>\d\d(?<gender>\d))(?<checksum>\d\d)$");
            var yyyy = RandomDateFunc().Invoke();
            string formatOut = "ddMMyy";
            var date = yyyy.ToString(formatOut);

            int d1 = Convert.ToInt32(date.Substring(0, 1));
            int d2 = Convert.ToInt32(date.Substring(1, 1));
            int m1 = Convert.ToInt32(date.Substring(2, 1));
            int m2 = Convert.ToInt32(date.Substring(3, 1));
            int y1 = Convert.ToInt32(date.Substring(4, 1));
            int y2 = Convert.ToInt32(date.Substring(5, 1));
            int c1 = 0, c2 = 0, i1 = 0, i2 = 0, i3 = 0;

            do{ //Find mathing pairs of c1 and c2
                //c2 need to be equal to last digit.
                var r = new Random();
                var num = r.Next(100, 499); //Wrong but here we dont need perfect numbers since all are fake.
                int[] n = num.ToString().ToCharArray().Select(c => Convert.ToInt32(c.ToString())).ToArray();
                i1 = n[0]; i2 = n[1]; i3 = n[2];

                //First num (See wiki)
                var v1 = (3 * d1) + (7 * d2) + (6 * m1) + m2 + (8 * y1) + (9 * y2) + (4 * i1) + (5 * i2) + (2 * i3);
                c1 = (v1 % 11) == 0 ? 0 : 11 - (v1 % 11);

                //Second/last num (See wiki)
                int v2 = (5 * d1) + (4 * d2) + (3 * m1) + (2 * m2) + (7 * y1) + (6 * y2) + (5 * i1) + (4 * i2) + (3 * i3) + (2 * c1);
                c2 = (v2 % 11) == 0 ? 0 : 11 - (v2 % 11);

                //Do until c1 and c2 is lower than 10 and both are true.
                if ((c1 < 10) && (c2 < 10))                    
                    break;

            } while (true);

            var ssn = $"{d1}{d2}{m1}{m2}{y1}{y2}{i1}{i2}{i3}{c1}{c2}";
            var match = rex.Match(ssn);
            var gender = int.Parse(match.Groups["gender"].Value);
            var sex = gender % 2 == 0 ? "Male" : "Female"; // If wanted
            //Console.WriteLine(sex);
            var s = new SSN(sex){ Number = ssn, Gender = sex };
            
            return s;
        }

    }


    /// <summary>
    /// Holder of a Person data
    /// </summary>
    public class SSN
    {
        public string Number { get; set; }
        public string Gender { get; set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }
    	
	//<----- Thousends of combinations of names.. See code on my space on bitbucket

        public string FirstNameMale = @"Jan,Per,Bjørn,Ole,Lars,Kjell,Knut";
        public string FirstNameFemale = @"Anne,Inger,Kari,Marit,Ingrid,Liv,Eva"; 
        public string LastNames = @"Hansen,Johansen,Olsen,Larsen"; 


        /// <summary>
        /// Creates a new instance of SSN
        /// </summary>
        /// <param name="gender">The gender to generate.</param>
        public SSN(string gender)
        {
            var rand = new Random();

            if (gender == "Male")
                FirstName = FirstNameMale.Split(',')[rand.Next(0,49)];
            if (gender == "Female")
                FirstName = FirstNameFemale.Split(',')[rand.Next(0, 49)];

            LastName = LastNames.Split(',')[rand.Next(0, 3438)];
        }
    }`
            },
            {
                slug: "nuxt-vue-static-site",
                title: "The Future site",
                content: "<h2>Build the ultimate static site with minimal server resources used. Some thoughts on my next site, and to augment the power of a static site.</h2><br/><img src='/static/img/nuxt.svg' class='contentImg' alt='Nuxt JS'/><br/><br/><br/>Somewhat I’m not satisfied with the layout/code splitting in React, it works, but it can easily become nested and hard to read. I would rather split code and views.The way to update content, since I use Next JS exported as a static site. But I do like the performance! When it comes to CSS it’s an easy choice, less or sass, here we split code into separate files and it easy to find.<br/><br/> Yet to find out is if Vue can be at help. I’ve been reading about both Nuxt and Vue and I think I going to make a small project to see if this is the thing that I’m looking for.<br/><br/>Somehow a site that I static doesn’t have any problems with databases and other stuff that can make the site go down. This is also a cost issue.<br/><br/><h2>The ideal site construction!</h2>Is there any?<br/>Static, but dynamic when it comes to add content. How? When I make a new page when it also needs to re-render pages because everything is static. A trigger? If possible run the site without server rendering of pages? Create a trigger on changes...<br/>Clean links and meta tags.<br/>Split views(templates) and code<br/>Only update site on API changes.<br/>Run the site as a SaaS app.<br/> Run the site with low cost.<br/>Be modular, adding modules on the fly.<br/>Cache, for example Cloudflare’s.<br/>Everything thing needs to accessible online.<br/><br/><h2>Keep the discussion alive!</h2>I need to make a small forum to hear about what others think is a solid and good way to build and create a solid solution that takes a minimal of resources.<br/><br/><h2>So what it the optimal site?</h2>For me this is not an easy question. But I know what I want. I want a static site that re- renders when API changes. I want the API data to be generated on changes to a static file or cache. Trigger changes to app to generate pages.<br /> <br />In my site it’s easy to add a module.Create the module in code, then add data to it in my WebAPI. I like this way, it’s not complicated. It also give me choice to put content where I like it to be, just thru code.<br /> <br /><h2>XML database</h2><br/>In the past I’ve built a lot of CMS’s and tools. The one I started with had xml as database. Oh man, was it fast. Maybe I can implement a way of reusing this engine to use for my current application.<br /> <br /><h2>What about Nuxt?</h2><br/>What is it? It mostly behave like Next JS, with the difference from the view engine. I’m already a big fan of Next, can Nuxt make me feel happy when I structure my application? This is what I need to find out.<br/><br/>For now, if anyone has any idea on how to accomplish this, please send me an email with your thoughts.<br/>I found an interresting article on the subject where he compares the differences between Vue Js and React JS. See the article <a target='_blank' class='contentlink' href='https://medium.com/javascript-in-plain-english/i-created-the-exact-same-app-in-react-and-vue-here-are-the-differences-2019-edition-42ba2cab9e56'>here</a>",
                meta_title: "Next up, Nuxt and Vue templates",
                meta_description: "Thoughts on Nuxt and Vue templates and make the optimal static site.",
                layout: "/projects",
                template: "template.projects",
                created: "2019-10-18"
            },
            {
                slug: "deploy-react-azure-webapp",
                title: "Deploy React site to an Azure Web App",
                content: "<img src='/static/img/azure-deploy.png' alt='Azure deploy' class='contentimg'/><br/><br/> Iv'e been using my server in Azure for several years now. But, today I desided to move my website made in React, Redux and Next site to an Web App in Azure. That was an easy task since most of the configurations was made during the setup. <br/><br/>A webapp will also reduce the cost of using Azure per month, so I like so say that this is a good desision to move away from a real server to an Azure WebApp.<br/><br/><h2>Configuration</h2><b>Step 1:</b> Create the web app as a Node Js app in Azure. <br/><br/><b>Step 2:</b> Add a deployment slot. I choosed FTP as this was the fastest way to publish my code. In the future I will make it from Docker or a Git repo.<br/><br/>After I saw my app was is up and running, I needed to add my domain.<br/><br/><b>Step 3:</b> Create DNS records in Azure. Just click add domain to the app, and you get instructions what you need to change or add to your DNS provider. That was the most easy DNS Iv'e been using in years. <br/><br/><b>Final step 4:</b> I'm using Cloudeflare that will provide me with https for my domain. I added a few records, cname and a TXT record in my DNS configuration. <br/></br /><br/>And voila!! There it was, published and ready for new vistors. <br/><em>Oh man, do I love Azure!!!</em>",
                meta_title: "Deploy your React, Next JS website to a Azure",
                meta_description: "How I deployed my React, Next JS website to a Azure web App in a few easy step.",
                layout: "/projects",
                template: "template.projects",
                created: "2019-10-16"

            },
            {
                slug: "react-nextjs-in-site-search",
                title: "Fast search engine for React with Nextjs",
                content: "<img src='/static/img/search-dark.png' alt='Search' class='contentimg'/><br/><br/> Since this is a small website it's easy to make a searchengine. I created a function that returns a list of searchresults. It's fast and it displays the result in milliseconds. Actually it act more like a filter, because the result is already there, it just needs to be filtered.<br/><br/>Try the searchengine <a href='/search' class='contentlink'>here</a>. See the full code on <a class='contentlink' href='https://bitbucket.org/rickardmagnusson/bootproject-on-next-js-and-redux/src/master/src/lib/searchresult.js' target='_blank'>Bitbucket.</a>",
                meta_title: "Create a fast searchengine for your React, Next JS website",
                meta_description: "How I built my searchengine in a react js application?",
                code: `
    //Prepare list for the search.
    var elements = [];
     
    this.state.data.content.map(c =>
        elements.push({ path: c.path, title: c.title, content: c.content.replace(/<[^>]*>?/gm, '') }));

    this.state.data.projects.posts.filter(c =>
        elements.push({ path: "/projects/" + c.slug, title: c.title, content: c.content.replace(/<[^>]*>?/gm, '') }));

    var searchResults = SearchResult(e, elements, 150);


//Search result class
const SearchResult = (q, list, excerptCount) => {

    var results = [];
    var result = filter(q, list);

    result.map(c =>
        results.push({ path: c.path, title: c.title, content: c.content.substr(0, excerptCount)+'...' }));

    return results;

    function escapeRegExp(s) {
        return s.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\$&");
    }

    function filter(q, list) {
        const words = q
            .split(/\s+/g)
            .map(s => s.trim())
            .filter(s => !!s);

        const hasTrailingSpace = q.endsWith(" ");
        const searchRegex = new RegExp(
            words
                .map((word, i) => {
                    if (i + 1 === words.length && !hasTrailingSpace) {
                        // The last word - ok with the word being "startswith"-like
                        return (?=.*\\b@{ escapeRegExp(word) });
                    } else {
                        // Not the last word - expect the whole word exactly
                        return (?=.*\\b@{ escapeRegExp(word) }\\b);
                    }
                })
                .join("") + ".+",
            "gi"
        );
        return list.filter(item => {
            return searchRegex.test(item.content);
        });
    }
};

export default SearchResult;`,
                layout: "/projects",
                template: "template.projects",
                created: "2019-10-12"
            },
            {
                slug: "deconta-project",
                title: "Deconta project, CMS and design",
                content: "<img class='icon-fluid img-fluid float-left' src='/static/img/deconta-fav.png' /> Deconta, a German based factory and seller of NPU, Negative pressure units. This company wanted to start sell their products in Sweden, but they didn't have a Swedish site. So, I needed to create a new design, including design elements, a website and a CMS to handle changes in content. I created a new CMS based on my Multitenancy Core project, which make it incredible easy to produce models, since the heavy use of nHibernate.<br/><br/>This was one of the design elements I made in Adobe Animate. <br/><div class='box cornered'><img class='img-fluid float-right' src='/static/img/anim.gif' alt='NPU Animation'/><div style='clear:both'></div></div> <br/><br/><h1>Final design</h1> <br/> <br/><img class='img-fluid' src='/static/img/deconta.png' /><br/>(Adobe Photoshop image)",
                meta_title: "Deconta project, CMS",
                meta_description: "Deconta design and cms implementation",
                layout: "/projects",
                template: "template.projects",
                created: "2019-10-10"
            }
            ,
            {
                slug: "apple-app-a-like-website",
                title: "How to appify your website for IPhone",
                content: "<img src='/static/img/homescreen.png' alt='Homescreen' class='contentimg'/><br/><br/> How  do you appify your website, when to save it to homescreen on your IPhone? You need some meta tags and a manifest.json. See below. You also need to provide some images, like the icon, splash, if you are planning on using that. See the full source code for the head <a class= 'active' href='https://bitbucket.org/rickardmagnusson/bootproject-on-next-js-and-redux/src/master/src/lib/meta.js'> here</a>. <br/> <br />If you managed to save and open it in fullscreen as an app you will see that even links inside will show in a 'back to website window', so the feeling is really like an App.",
                meta_title: "Bootproject on how to appify you website for IPhone",
                meta_description: "How  do you appify you website, when save it to homescreen on your IPhone?",
                code: `
    //Meta tags
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="msapplication-starturl" content="index.html" />
    <link rel="manifest" href="/static/manifest.json" />

    //manifest.json
    {
        "name": "Bootproject",
        "short_name": "Bootproject",
        "lang": "en-US",
        "start_url": "/index.html",
        "display": "standalone", //<---Important
        "icons": [
        {
            "src": "/static/img/apple-icon-144x144.png"
        }
        ]
    }`,
                layout: "/projects",
                template: "template.projects",
                created: "2019-09-10"
            }
            ,
            {
                slug: "next-routebuilder",
                title: "Routebuilder for Next Js",
                content: "Routerbuilder, or better named as route construction for Next JS exportPaths, is made by adding each route in components, modules and pages to a single array of elements. The magic happends when we export the routes. It gets autogenerated for each post in application. <a class='active' href='https://bitbucket.org/rickardmagnusson/bootproject-on-next-js-and-redux/src/master/src/lib/routebuilder.js' target='_blank'>See code</a>",
                meta_title: "Routebuilder for Next Js",
                meta_description: "How to create a Routebuilder for Next Js",
                code: `module.exports = {

    exportTrailingSlash: true,

    exportPathMap: async function () {
        return routes.RouteBuilder(); //<--------
    },
    generateBuildId: async () => {
        return process.env.NODE_ENV === 'production' ? 'production' : 'dev';
    },
    serverRuntimeConfig: { // Will only be available on the server side
        staticFolder: 'static'
    },
    publicRuntimeConfig: {
        DOMAIN: process.env.NODE_ENV === 'production' ? 
            'https://www.footer.se' : 'http://localhost:3000',
        staticFolder: 'static'
    }
};`,
                layout: "/projects",
                template: "template.projects",
                created: "2019-09-08"
            }
            ,
            {
                slug: "next-redux",
                title: "Bootproject on Next Js and Redux",
                content: "Current project produces Zones and modules from a web api. When started this project in pure React I didnt realize that it was all without control of meta and tags. To add a server based static site I used Next JS, and Redux as data provider. This made the whole site customizable from document to tags. I also wanted Zones to load modules dynamically. And as you see, it really works. See bitbucket for source.",
                meta_title: "Bootproject on next and redux",
                meta_description: "Next Js and redux development. Current project produces zones and modules dynamically from a web api.",
                code: "Example: <Zone name=´Content´ store={this.props.store} current={this.props.current}/>",
                layout: "/projects",
                template: "template.projects",
                created: "2019-09-01"
            },
            {
                slug: "meta-document-layout",
                title: "Clean tags with _document.js",
                content: "From the start when I used Next JS, I just hated the way the tags was generated. Unorganized, with classnames and other irrelevante stuff. By extending the _document.js and _app.js I could change the default behaviour of the rendering. Every tag is now how it surposed to be. See the changes <a class='active' href='https://bitbucket.org/rickardmagnusson/bootproject-on-next-js-and-redux/src/master/src/lib/meta.js'>here</a> Of cource you can hit F12 to se the result directly.",
                meta_title: "Bootproject on create cleaner meta tags with _app, and _document.js overriden",
                meta_description: "How to change the default behaviour of rendering meta tags in Next js by extending and override app and document js in next.",
                code: `
<React.Fragment>
    <Head>
        <meta charSet="utf-8" />
        {props.meta.title && <title>{props.meta.title}</title>}
        {props.meta.description && <meta name="description" 
            content={props.meta.description} />}
        <meta name="keywords" content={keywords()} />
    </Head>
<React.Fragment>

`,
                layout: "/projects",
                template: "template.projects",
                created: "2019-08-14"
            },
            {
                slug: "multitenancy",
                title: "Multitenancy Core 2.2",
                content: "Since its getting more common to use .Net Core projects, I needed to make the previous(4.6) Boot.Multitenancy project compatible with .Net Core. Boot.Multitenancy is a nHibernate extension that makes it very easy to use multiple domains and their databases. The new project is more lightweighted and is easy to install. Add two lines in Startup.cs, and your project is ready to server multiple sites. See the <u><a href='https://bitbucket.org/rickardmagnusson/bootproject.corev2/src/master/' class='active' target='_blank'>project page</a></u> on bitbucket for more information.",
                meta_title: "Multitenancy Core 2.2",
                meta_description: "Multitenancy Core 2.2 version of multitenancy for .Net core",
                layout: "/projects",
                template: "template.projects",
                created: "2019-08-01"
            },
            {
                slug: "azure-durable-functions",
                title: "Deployment with durable functions (V1)/(V2)",
                content: "<br/><br/><img src='/static/img/azure.png' class='contentimg' alt='Flow, Azure' /><br/>In this project I needed to take on premise services to Azure. Like any other project it is just not that, its so much more. First of is the project it self, its made in .Net and not in Core wish makes it a bit harder since Azure durable function only exits in .NET Core. <br/><br/> So after some research on how to achieve this, it failed because these services is connected to Dynamics CRM, and theres no support for Core in any way. (Not at this moment). Hmmm. How to proceed? I surely needed the durable function so I can control the services, so I decided to make the services call from Azure function V1, since they have support for .Net. So I created my V1 as .NET and the other V2 as a durable function, and the later one calls the V1 regular function with an async call. This is not an optimal solution, but still, it works.",
                meta_title: "Deployment with durable functions (V1)/(V2)",
                meta_description: "Deployment with durable functions (V1)/(V2)",
                layout: "/projects",
                template: "template.projects",
                created: "2019-04-01"
            },
            {
                slug: "dynamics-365-on-docker",
                title: "Dynamics 365, Visual studio Code, docker deployment",
                content: "In a few weeks, I need to be prepared to develop in Dynamics 365. Can you actually learn Dynamics in such short time? Well I need to. Well not all, thats impossible. I need to know the basics. I'm start working with a new client soon where we are going to pull services and applications to Azure. I started search Internet for information about develop in Dynamics.<br/><br/> <h2>Installation</h2>First, I tried to install it on a VM in Azure, but there were so much configurations that I left that idea. There must be a easier solution to develop in Dynamics. And there was. I ended up installing the image in Docker. Also I fired up Visual studio Code, and installed the AL extension. The big disadvantage of the image is the size, 18GB. The benefit is that you will have a container that runs out of the box.<br/><br/>It's pretty easy to install the image. Install Docker, and sign up at Docker hub. Run the command (docker run -e accept_eula=Y -m 4G microsoft/bcsandbox)(4G is how much memory you will use). When the installation is finished you'll get all required parameters in the console. Username, password, url etc in cmd. And its ready to get fired up. Open the browser and install the .cer file(displayed in the cmd) and continue to Dynamics 365 Central, Neat Microsoft and Docker!!. Now your'e ready to deploy new code to Dynamics 365.<br/><br/><h2>AL (Application language)</h2>It seams sooo.. Microsoft to develop a new language. Reminds me of my time developing in Sharepoint. Anyhow, it seems quite straight forward, and to deploy to the container also worked as expected.<br/><br/><h2>Resources</h2><a class='active' href='https://hub.docker.com/r/microsoft/bcsandbox/' target='_blank'>Dynamics Docker 365 image</a>",
                meta_title: "Dynamics 365, Visual studio Code, docker deployment",
                meta_description: "Dynamics 365, Visual studio Code, docker deployment",
                layout: "/projects",
                template: "template.projects",
                created: "2019-03-01"
            },
            {
                slug: "docker-kubernetes",
                title: "Azure AKS, Kubernetes deployment",
                content: "This week I've tested out the Azure AKS, Kubernetes (container) in Azure. But man, is that a lot of configurations to do!! I followed a blogpost (<a class='active' href='https://blogs.msdn.microsoft.com/azuredev/2018/03/27/building-microservices-with-aks-and-vsts-part-1' target='_blank'/>AKS VSTS</a> ) that a collegue of mine did, Andreas Helland, a blogpost at Microsoft on creating AKS in devOps(VSTS). As a former collegue of mine said about DevOps, - Is how to trick developers to work with deployment. So they tricked me as well, I guess LoL :-).<br/><br/>Im not kidding when I say that steps that has to be made.. Uhhh A lot!! Andreas addressed me on this...<br/><br/>Theres a lot of things you need to know before you get started. Azure CLI, Docker, Helm, Tiller, Hyper-V, Kubernetes, Kubernetes dashboard and ofcource Azure. I managed to create a lot of errors during the lesson, but as always, errors makes you stronger and better as a developer and also make you a better problem solver.. Aint right?<br/><br/> -Anyhow, I manage to get thrue all the steps and the only thing left is to make it public, with an A record in DNS. So I ended up to buying a new domain(amazingly available) www.footer.se. So hopefully you will see the project I made, end up in that domain. <br/><br/>We have to say thanks to Andreas Helland that made the blogpost!! Without your post, I would have been totally lost!!<br/><img src='/static/img/kubernetes-azure.png' class='contentimg' alt='Kubernetes, Azure(AKS)' /><br/><br/>",
                meta_title: "Azure AKS, Kubernetes deployment",
                meta_description: "Azure AKS, Kubernetes deployment development tests",
                layout: "/projects",
                template: "template.projects",
                created: "2019-02-20"
            },
            {
                slug: "cloud-flare-dns",
                title: "Cloudflare offers you free SSL and much more",
                content: "Since Google announced that they only will include or prioritize websites that used SSL, I investigated if there was a free service since I own a couple of domains that could offer free HTTPS. I found the Cloudflare service, with free SSL certificates. Cloudflare offers an easy way to manage your DNS thrue their controlpanel. This must be the website service of the year, the service is so good that even Alltin.no used it when they released the Skatteopp&#248;r(tax Assessment in Norway) for the Norwegian peoples. You may wonder why, but it's because of the cache service they provide. You can actually turn off you site and just run it from their cache. Smooth isn't it. So head over to cloudflare and sign up for your free service. <a class='active' href='https://www.cloudflare.com/' target='_blank'>Cloudflare</a>  ",
                meta_title: "Cloudflare",
                meta_description: "Cloudflare offers you free SSL and much more.",
                layout: "/projects",
                template: "template.projects",
                created: "2019-02-10"
            }
            ,
            {
                slug: "azure-tables",
                title: "Azure tables, Storage",
                content: "Today ive tested out the Azure Table feature. In a simple, fast and secure way you can put content on the cloud and serve it as an API. Check it out!",
                meta_title: "Azure tables, Storage",
                meta_description: "Azure tables, Storage.",
                layout: "/projects",
                template: "template.projects",
                created: "2019-01-20"
            },
            {
                slug: "sanity",
                title: "Sanity react and GROQ",
                content: "When I started to run React applications, I run across Sanity.io. They offers a complete solution thrue Node. A frontend and a headless backend that is installed in minits. As they descibes their self - Build headless, collaborative editing environments in React.js. They also developed a query language, GROQ that is a bit similar to GraphQL. Ive tested out the applications and their query editor. They have an really good idea here, easy to install and extend thrue the scheme. Good work Sanity.",
                meta_title: "Sanity react and GROQ",
                meta_description: "Sanity react and GROQ.",
                layout: "/projects",
                template: "template.projects",
                created: "2019-01-05"
            }
            ,
            {
                slug: "creating-react-components",
                title: "Creating React Components",
                content: "<h2>This is about create React JS components.</h2> The main thing with React is how easy it is to learn. And the performance is awesome!!<br/><br/> If you understand the layout and continue making your own extensions, it will result in a application that is very fast to build. At least, thats my experience.",
                meta_title: "Creating React Components",
                meta_description: "Creating React Components",
                layout: "/projects",
                template: "template.projects",
                created: "2018-10-12"
            },
            {
                slug: "project-module",
                title: "Project module",
                content: "<h2>A module that shows my current projects</h2> This module is about learning subroutes, and how React are using Routes. What you are seing on this page is the project module created. It also has a codearea, on where to show code from projects.",
                meta_title: "Project module",
                meta_description: "Creatíng a Project module in React Js",
                layout: "/projects",
                template: "template.projects",
                created: "2018-10-10"
            }
        ]
    }
    ,
    //Timeline items
    timeline: {
        id: "1",
        path: "/",
        zone: "Content",
        style: "",
        title: "Bootproject timeline",
        content: "Latest projects and assignments",
        list: [
            {
                title: "Duvi Pensjon",
                content: "Current freelance project is at Duvi Pensjon in Norway. An interresting project with various assaignments. Development of async functions in a none async project, to be able to download and process millions rows of data. Develop a Bitbucket Api to connect and clone git source, to later on deploy to servers from the publish profiles."
            }
            ,
            {
                title: "Headless CMS",
                content: "For this site I'm going to use a headless CMS from Sanity. A really impressive headless CMS when it comes to setup thru schemes and the editing of a site. It creates instant updates to a NextJS site.<a class='contentlink' href='/projects/build-a-cms-with-sanity/'>Read more about Sanity here</a>"
            }
            ,
            {
                title: "Business Rules",
                content: "On how to solve complex function results, and make it understandable for everyone. <a class='contentlink' href='/projects/business-rules-csharp-part-2/'>See the article here</a>"
            }
            ,
            {
                title: "Syntaxhighlighter",
                content: "Needed a codeviewer to display code in pages so I added Syntaxhighlighter. Simple, easy and clean code coloring views."
            }
            ,
            {
                title: "Next Js and Redux",
                content: "Developing more on this site to learn Next js and Redux, and how to to make it more SEO friendly."
            }
            ,
            {
                title: "Dynamics 365",
                content: "Here Im preparing for a new project for a bank institution. This client that need to move all projects from Dynamics On Premise to Azure. Including Windows services to Azure functions. <a href='/projects/azure-durable-functions/' class='contentlink'>Read more about the project here</a>"
            }
            ,
            {
                title: "Kubernetes",
                content: "KUBERNETES... Yummy. Can you taste the word? -Anyhow, my current goal is to create microservices with Kubernetes and to understand the Kubernetes and Docker pipeline."
            }
            ,
            {
                title: "Azure tables, Storage",
                content: "Build your API or content structure thrue Azure tables. An pretty easy way to put your api online in a secure way."
            }
            ,
            {
                title: "Multitenancy V2 .Net Core",
                content: ".NET package that makes ease of using multiple domains and databases. Download at NuGet. -pm> install-package Boot.Multitenancy"
            }
        ]
    }
};

module.exports = Service;
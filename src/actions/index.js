﻿const initialApi = require("../middlewares/index");
import { SET_FETCH_DATA } from "../types";

export function fetchData() {
    return function (dispatch) {
        return initialApi
            .then(({ data }) => {
                dispatch(setData(data));
            });
    };
}

function setData(data) {
    return {
        type: SET_FETCH_DATA,
        payload: data
    };
}
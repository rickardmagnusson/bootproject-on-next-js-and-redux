﻿import App from 'next/app';
import React from 'react';
import withReduxStore from '../lib/with-redux-store';
import { Provider } from 'react-redux';

class MyApp extends App {
    render() {
        const { Component, pageProps, reduxStore, query } = this.props;

        return (
            //Container is deprecated </Container>
            <Provider store={reduxStore}>
                <Component {...pageProps} route={query} store={reduxStore} />
            </Provider>
        );
    }
}

export default withReduxStore(MyApp);
﻿import React from 'react';
import { connect } from 'react-redux';
import Layout from "../lib/layout";
import MobileNav from "../components/mobile";
import Intro from "../components/intro";
import Footer from "../components/footer";
import Zone from "../components/zone";

class Index extends React.Component {

    static getInitialProps = async ({ query, reduxStore }) => {

        const store = reduxStore.getState();
        const page = store.pages.find(p => p.path === query.path);

        /*
           Create data, meta for the application
         */
        const data = {
            meta: {
                lang: 'en',
                title: page.meta_title,
                description: page.meta_description
            },
            id: query.path,
            current: page,
            layout: page.layout,
            routes: store.pages,
            store: store,
            subRoutes: null,
            useReact: true
        };
        return data;
    }


    render() {
        return (
            <Layout meta={this.props.meta} current={this.props.current}>
                <MobileNav route={this.props.id} routes={this.props.routes} subRoutes={this.props.subRoutes} />
                {this.props.id === "/" ? <Intro title={this.props.meta.title} /> : ""}
                <Zone name="BeforeContent" store={this.props.store} current={this.props.current} />
                <Zone name="Content" store={this.props.store} current={this.props.current} />
                <Zone name="AfterContent" store={this.props.store} current={this.props.current} />
                <Footer />
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({ pages: state.pages });
export default connect(mapStateToProps)(Index);
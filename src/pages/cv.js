﻿import React from 'react';
import { connect } from 'react-redux';
import Layout from "../lib/layout";
import MobileNav from "../components/mobile";
import Intro from "../components/intro";
import Footer from "../components/footer";
import Zone from "../components/zone";
import CV from "../components/cv";

class Index extends React.Component {

    static getInitialProps = async ({ query, reduxStore }) => {

        const store = reduxStore.getState();
        const page = store.pages.find(p => p.path === query.path);

        /*
           Create data, meta for the application
         */
        const data = {
            meta: {
                lang: 'en',
                title: "cv",
                description: "Cv description"
            },
            id: query.path,
            current: page,
            layout: page.layout,
            useReact: true
        };
        return data;
    }


    render() {
        return (
            <Layout meta={this.props.meta}>
                <CV />
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({ pages: state.pages });
export default connect(mapStateToProps)(Index);
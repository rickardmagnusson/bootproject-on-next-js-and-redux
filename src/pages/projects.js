import React from 'react';
import { connect } from 'react-redux';
import Layout from "../lib/layout";
import Project from "../components/project";
import Footer from "../components/footer";
import MobileNav from "../components/mobile";
import { fetchData } from "../actions";
/*
    Component project, with list of project
 */
class Projects extends React.Component {

    static getInitialProps = async ({ query, reduxStore }) => {

        const store = reduxStore.getState();
        const pages = store.pages;
        const posts = store.projects.posts;
        var page = pages.find(p => p.path === query.path);

        if (query.pathId) {
            page = posts.find(p => p.slug === query.pathId);
        }

        return {
            meta: {
                lang: 'en',
                title: page &&  page.meta_title,
                description: page && page.meta_description
            },
            layout: page && page.layout,
            id: query.pathId,
            path: query.path,
            template: page && page.template,
            routes: store.pages,
            posts: posts,
            subRoutes: store.projects,
            useReact: false
        };
    }

    /*
     * Get the current project.
     * If no project is the current, return the last in list.
     */
    getProject = () => {
        if (!this.props.id)
            return this.props.projects.posts.filter(p => p)[0];
        return this.props.projects.posts.filter(p => p.slug === this.props.id)[0];
    }

    render() {

        return (
            <Layout meta={this.props.meta} current={this.props.path} template={this.props.layout}>
                <MobileNav route={this.props.path} routes={this.props.routes} subRoutes={this.props.subRoutes} />
                <Project props={this.getProject()} route={this.props.route} posts={this.props.posts} title="Projects" />
                <Footer />
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({ pages: state.pages, projects: state.projects });
export default connect(mapStateToProps)(Projects);
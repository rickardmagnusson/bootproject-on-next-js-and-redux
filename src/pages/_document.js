﻿import React from 'react';
import Document, { Main, NextScript } from 'next/document';
import Header from "../lib/header";

export default class CustomDocument extends Document {

    static async getInitialProps(ctx) {

        const initialProps = await Document.getInitialProps(ctx);

        return { ...initialProps };
    }

    render() {

        const props = this.props.__NEXT_DATA__.props.pageProps;
        const useReact = !!props.useReact;

        return (

            <html>
                <Header />
                <body>
                    <div id="root">
                        <section>
                            <Main className="container" />
                        </section>
                    </div>
                    <NextScript />
                    <script src="/static/js/jquery-1.10.2.min.js" />
                    <script src="/static/js/bootstrap.min.js" />
                    <script src="/static/js/bootproject.config.js" />
                    <script src="/static/js/embed.js" />
                </body>
            </html>
        );
    }
}
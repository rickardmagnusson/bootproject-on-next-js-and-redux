﻿(window.webpackJsonp = window.webpackJsonp || []).push([
    [5], {
        RNiq: function (t, e, n) {
            "use strict";
            n.r(e);
            var a = n("UrUy"),
                r = n.n(a),
                s = n("R3/3"),
                o = n("LkAs"),
                i = n("Moms"),
                c = n("bMj6"),
                l = n("hZod"),
                u = n("tEuJ"),
                p = n("azxR"),
                m = n("mXGw"),
                h = n.n(m),
                d = n("/m4v"),
                v = n("jvst"),
                f = n("MbLX"),
                b = h.a.createElement,
                g = function (t) {
                    return b("div", {
                        className: "sectionroot"
                    }, b("section", null, b("div", {
                        className: "sectioncontent"
                    }, b("div", {
                        className: "sharedSection"
                    }, b("h1", null, b("span", null, "Boot"), b("span", null, "project "), b("span", null, "-developer"), b("span", null, "connection")), b("div", {
                        className: "imagecontainer"
                    }, b("img", {
                        alt: "Logo back",
                        src: "/static/img/logobg.svg"
                    })), b("div", null, b("p", {
                        className: "intro"
                    }, "Bootproject is development base for all projects and also where to create and test out new features from both .NET, javascript frameworks and more.", b("br", null)), b("a", {
                        className: "biglink",
                        href: "https://bitbucket.org/rickardmagnusson/bootproject-on-next-js-and-redux/src/master/",
                        rel: "noopener noreferrer",
                        target: "_blank"
                    }, "Grab your copy V2 on bitbucket"))))))
                },
                j = n("aIN1"),
                N = n("qqQO"),
                y = h.a.createElement,
                O = function (t) {
                    function e(t) {
                        var n, a = t.props;
                        return Object(o.a)(this, e), (n = Object(c.a)(this, Object(l.a)(e).call(this))).state = {
                            props: a
                        }, n
                    }
                    return Object(u.a)(e, t), Object(i.a)(e, [{
                        key: "render",
                        value: function () {
                            return this.state.props.list.length > 0 ? y("div", {
                                className: "sectionroot"
                            }, y("section", null, y("div", {
                                className: "sharedSection" + this.state.props.style
                            }, y("div", {
                                className: "sectioncontent"
                            }, y("header", null, y("img", {
                                src: this.state.props.icon,
                                alt: "multi",
                                className: "big"
                            }), " ", y("h2", null, this.state.props.title)), y("ul", {
                                className: "shared_grid"
                            }, this.state.props.list.map(function (t) {
                                return y("li", null, y("h2", null, y("img", {
                                    src: t.icon,
                                    alt: "multi",
                                    className: "small"
                                }), " ", t.title), y("p", {
                                    dangerouslySetInnerHTML: {
                                        __html: t.content
                                    }
                                }), y("br", null), y("br", null), t.code && y(N.a, {
                                    language: "javascript"
                                }, t.code))
                            })))))) : y("div", {
                                className: "sectionroot " + this.state.props.style
                            }, y("section", null, y("div", {
                                className: "sharedSection"
                            }, y("div", {
                                className: "sectioncontent"
                            }, y("header", null, y("img", {
                                src: this.state.props.icon,
                                alt: "multi",
                                className: "mini"
                            }), " ", y("h2", null, this.state.props.title)), y("span", {
                                dangerouslySetInnerHTML: {
                                    __html: this.state.props.content
                                }
                            }), y("br", null), y("br", null), this.state.props.code && y(N.a, {
                                language: "javascript"
                            }, this.state.props.code)))))
                        }
                    }]), e
                }(h.a.Component),
                k = h.a.createElement,
                w = {
                    Content: O,
                    Timeline: function (t) {
                        var e = t.props;
                        return k("section", null, k("div", {
                            className: "sharedSection"
                        }, k("div", {
                            className: "sectioncontent"
                        }, k("div", {
                            class: "timeliner"
                        }, k("header", null, k("h2", null, e.title), k("div", null, e.content)), e.list.map(function (t) {
                            return k("div", {
                                class: "timeline-point"
                            }, k("div", {
                                class: "timeline-icon"
                            }), k("div", {
                                class: "timeline-block"
                            }, k("div", {
                                class: "timeline-content"
                            }, k("h3", null, t.title), k("p", null, t.content))))
                        })))))
                    }
                },
                E = function (t) {
                    function e(t) {
                        var n, a = t.name,
                            r = t.current,
                            s = t.store;
                        Object(o.a)(this, e), n = Object(c.a)(this, Object(l.a)(e).call(this));
                        var i = s.getState();
                        return n.state = {
                            zone: a,
                            page: r,
                            data: i
                        }, n
                    }
                    return Object(u.a)(e, t), Object(i.a)(e, [{
                        key: "renderElement",
                        value: function (t, e) {
                            return h.a.createElement(w[t], {
                                props: e
                            })
                        }
                    }, {
                        key: "recurseElements",
                        value: function (t, e) {
                            return this.renderElement(t, e)
                        }
                    }, {
                        key: "getZoneDataForPage",
                        value: function (t) {
                            var e = this,
                                n = [],
                                a = this.state.data[t.toLowerCase()];
                            return "[object Array]" === Object.prototype.toString.call(a) ? a.map(function (t) {
                                t.zone === e.state.zone && t.path === e.state.page.path && n.push(t)
                            }) : a.zone === this.state.zone && a.path === this.state.page.path && n.push(a), n
                        }
                    }, {
                        key: "createZoneElements",
                        value: function (t) {
                            var e = [];
                            for (var n in t)
                                if (t[n].name === this.state.zone) {
                                    var a = t[n].modules.map(function (t) {
                                        return t
                                    });
                                    for (var r in a) {
                                        var s = this.getZoneDataForPage(a[r]);
                                        for (var o in s) e.push(this.recurseElements(a[r], s[o]))
                                    }
                                }
                            return e
                        }
                    }, {
                        key: "render",
                        value: function () {
                            var t = this.state.page,
                                e = this.createZoneElements(t.zones);
                            return h.a.createElement(h.a.Fragment, {
                                props: this.props
                            }, e)
                        }
                    }]), e
                }(h.a.Component),
                _ = h.a.createElement,
                C = function (t) {
                    function e() {
                        return Object(o.a)(this, e), Object(c.a)(this, Object(l.a)(e).apply(this, arguments))
                    }
                    return Object(u.a)(e, t), Object(i.a)(e, [{
                        key: "render",
                        value: function () {
                            return _(v.a, {
                                meta: this.props.meta
                            }, _(f.a, {
                                route: this.props.id,
                                routes: this.props.routes
                            }), "/" === this.props.id ? _(g, {
                                title: this.props.meta.title
                            }) : "", _(E, {
                                name: "BeforeContent",
                                store: this.props.store,
                                current: this.props.current
                            }), _(E, {
                                name: "Content",
                                store: this.props.store,
                                current: this.props.current
                            }), _(E, {
                                name: "AfterContent",
                                store: this.props.store,
                                current: this.props.current
                            }), _(j.a, null))
                        }
                    }]), e
                }(h.a.Component);
            Object(p.a)(C, "getInitialProps", function () {
                var t = Object(s.a)(r.a.mark(function t(e) {
                    var n, a, s, o, i;
                    return r.a.wrap(function (t) {
                        for (; ;) switch (t.prev = t.next) {
                            case 0:
                                return n = e.query, a = e.reduxStore, s = a.getState(), o = s.pages.find(function (t) {
                                    return t.path === n.path
                                }), i = {
                                    meta: {
                                        lang: "en",
                                        title: o.meta_title,
                                        description: o.meta_description
                                    },
                                    id: n.path,
                                    current: o,
                                    layout: o.layout,
                                    routes: s.pages,
                                    store: s
                                }, "/multitenancycore" === n.path && s.content.find(function (t) {
                                    return "/multitenancycore" === t.path && "Content" === t.zone
                                }), t.abrupt("return", i);
                            case 6:
                            case "end":
                                return t.stop()
                        }
                    }, t)
                }));
                return function (e) {
                    return t.apply(this, arguments)
                }
            }());
            e.default = Object(d.b)(function (t) {
                return {
                    pages: t.pages
                }
            })(C)
        },
        TqC3: function (t, e, n) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/", function () {
                var t = n("RNiq");
                return {
                    page: t.default || t
                }
            }])
        }
    },
    [
        ["TqC3", 1, 0]
    ]
]);